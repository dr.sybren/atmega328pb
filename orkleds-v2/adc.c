/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "adc.h"
#include "hardware.h"

#include <avr/io.h>

static uint16_t adc_read(uint8_t mux);
static void wait_until_conversion_done(void);
static void start_conversion(void);

void adc_setup()
{
  ADMUX = 0                 //
          | 0b01 << REFS0   // AVcc as ref voltage
          | 0 << ADLAR      // left-adjust on/off
          | 0b1111 << MUX0  // Read from GND by default.
      ;

  ADCSRA = 0            //
           | 1 << ADEN  // Enable ADC
#if F_CPU == 8000000
           | 0b110 << ADPS0  // 1/64 clkdiv for 125 KHz
#elif F_CPU == 1000000
           | 0b011 << ADPS0  // 1/8 clkdiv for 125 KHz
#else
#  error No case to handle this F_CPU value
#endif
      ;

  ADCSRB = 0;
  DIDR0 = 0             // Disable digital input on ADC pins
          | 1 << ADC1D  //
          | 1 << ADC2D  //
          | 1 << ADC3D  //
      ;
}

uint16_t adc_read_green()
{
  return adc_read(2);  // ADC2
}
uint16_t adc_read_white()
{
  return adc_read(1);  // ADC1
}
uint16_t adc_read_vref()
{
  return adc_read(3);  // ADC3
}

static uint16_t adc_read(const uint8_t mux)
{
  wait_until_conversion_done();

  const uint8_t mask = 0b1111 << MUX0;
  ADMUX = (ADMUX & ~mask) | (mux << MUX0);

  start_conversion();
  wait_until_conversion_done();

  return ADC;
}

static void start_conversion()
{
  ADCSRA |= 1 << ADSC;
}

static void wait_until_conversion_done()
{
  while (ADCSRA & (1 << ADSC))
    ;
}
