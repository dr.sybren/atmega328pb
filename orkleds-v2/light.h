/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

void light_setup(void);

/** Set visual intensity value on the LED.
 * 0: off, 1-1023: on at various PWM levels.
 * Return PWM value used for the light. */
int16_t light_visual_green(int16_t visual_intensity, float dimming_factor);
int16_t light_visual_white(int16_t visual_intensity, float dimming_factor);

#ifdef __cplusplus
}
#endif
