/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "state.h"
#include "adc.h"
#include "hardware.h"
#include "interrupts.h"
#include "thermometer.h"

MachineState state;

static void update_temperature();

void state_setup()
{
  state.dimming_factor = 1.0f;
}

void state_update()
{
  state.adc_white = adc_read_white();
  state.adc_green = adc_read_green();
  state.adc_vref = adc_read_vref();

  update_temperature();
}

static void update_temperature()
{
  static uint8_t last_time_mod100 = 0;
  const uint8_t time_mod100 = time_centiseconds % 100;

  if (time_mod100 == last_time_mod100) {
    return;
  }

  if (time_mod100 == 0) {
    thermometer_start_measurement();
  }
  else if (time_mod100 == 40) {
    MCP9808_temp measured = thermometer_read_measurement();
    state.temperature = measured.temperature;
    if (measured.ta_vs_tcrit) {
      state.temperature_alert = ALERT_CRITICAL;
    }
    else if (measured.ta_vs_tupper) {
      state.temperature_alert = ALERT_UPPER;
    }
    else if (measured.ta_vs_tlower) {
      state.temperature_alert = ALERT_LOWER;
    }
    else {
      state.temperature_alert = ALERT_NONE;
    }
  }

  last_time_mod100 = time_mod100;
}
