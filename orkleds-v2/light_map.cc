/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "light_map.h"

#include <stdint.h>

namespace {
#if LIGHT_BITS == 10
constexpr light_pwm_value_t light_map[] = {
    1023, 1022, 1021, 1020, 1019, 1018, 1016, 1015, 1012, 1010, 1007, 1003, 998, 992, 985, 977,
    967,  955,  940,  922,  900,  874,  842,  803,  756,  700,  631,  548,  448, 326, 178, 0,
};
constexpr light_pwm_value_t light_max_value = 1023;
#elif LIGHT_BITS == 8
static const light_pwm_value_t light_map[] = {
    255, 254, 253, 252, 251, 250, 249, 248, 247, 245, 244, 242, 239, 237, 233, 230,
    226, 221, 216, 210, 202, 194, 185, 174, 161, 147, 130, 111, 88,  63,  34,  0,
};
constexpr light_pwm_value_t light_max_value = 255;
#else
#  error "Unsupported number of bits"
#endif

static_assert(sizeof(light_map) / sizeof(light_map[0]) < 256, "light_map is too large");
constexpr uint8_t light_map_size = sizeof(light_map) / sizeof(light_map[0]);
constexpr uint8_t light_map_max_index = light_map_size - 1;
}  // namespace

/**
 * Calculate the PWM value for a linear input value between 0 and 1.
 */
light_pwm_value_t light_map_lookup(const float value /* range [0, 1] */)
{
  if (value <= 0.0f) {
    return light_map[0];
  }

  const float index = value * light_map_max_index;
  const uint8_t lower_index = index;
  if (lower_index >= light_map_max_index) {
    return light_map[light_map_max_index];
  }

  const light_pwm_value_t lower_light_map_value = light_map[lower_index];
  const light_pwm_value_t upper_light_map_value = light_map[lower_index + 1];

  const float alpha = index - lower_index;
  const float interpolated = (1.0f - alpha) * lower_light_map_value +
                             alpha * upper_light_map_value;
  return interpolated + 0.5f;
}
