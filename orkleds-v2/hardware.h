/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#pragma once

#include <avr/io.h>
#include <stdbool.h>

#define F_CPU 8000000

// Pinout:
// PB1: OUT PWM-White / OC1A
// PB2: OUT PWM-Green / OC1B
// PC0: IN  BUTTON / PCINT8
// PC1: INA ADC-White / ADC1
// PC2: INA ADC-Green / ADC2
// PC3: INA VLEDREF / ADC3
// PD0: IN  UART-RX
// PD1: OUT UART-TX
// PD2: IN  TEMP-ALERT (open drain) / PCINT11
// PD5: OUT LED-Aux / OC0B

// Map ATmega328p register names to ATmega328pb ones.

// Use TWI port 0
#define TWBR TWBR0
#define TWSR TWSR0
#define TWAR TWAR0
#define TWDR TWDR0
#define TWCR TWCR0
#define TWAMR TWAMR0

static inline void led_aux(const bool enable)
{
  if (enable) {
    PORTD |= (1 << PORTD5);
  }
  else {
    PORTD &= ~(1 << PORTD5);
  }
}

static inline void led_white(const bool enable)
{
  // White LED has high = off, low = on.
  if (!enable) {
    PORTB |= (1 << PORTB1);
  }
  else {
    PORTB &= ~(1 << PORTB1);
  }
}

static inline void led_green(const bool enable)
{
  // Green LED has high = off, low = on.
  if (!enable) {
    PORTB |= (1 << PORTB2);
  }
  else {
    PORTB &= ~(1 << PORTB2);
  }
}

static inline bool is_pushbutton_pressed(void)
{
  return (PINC & (1 << PINC0)) == 0;
}

static inline bool is_temp_alert(void)
{
  // Temp alert is an open drain, so low = alert, high = no alert.
  return (PIND & (1 << PIND2)) == 0;
}
