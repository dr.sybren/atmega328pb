/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "buffer.hh"

#include <string.h>

uint8_t CircularBuffer::space() const
{
  if (buf_data_head_ <= buf_queue_head_) {
    // Not wrapped.
    return buf_size_ - (buf_queue_head_ - buf_data_head_) - 1;
  }
  // Wrapped.
  return buf_data_head_ - buf_queue_head_ - 1;
}

bool CircularBuffer::is_empty() const
{
  return buf_queue_head_ == buf_data_head_;
}

void CircularBuffer::queue_byte(uint8_t byte)
{
  while (space() < 1)
    __asm__ __volatile__("nop");

  // Push byte onto queue
  *buf_queue_head_ = byte;
  ++buf_queue_head_;
  if (buf_queue_head_ - buffer_ == buf_size_)
    buf_queue_head_ = buffer_;
}

pair<uint8_t, bool> CircularBuffer::pop_byte()
{
  if (is_empty())
    return pair{static_cast<uint8_t>(0), false};

  const uint8_t popped = *buf_data_head_;
  buf_data_head_++;

  if (buf_data_head_ - buffer_ == buf_size_)
    buf_data_head_ = buffer_;

  return pair{popped, true};
}
