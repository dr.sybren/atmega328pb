/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "hardware.h"
#include "interrupts.h"
#include "light.h"
#include "setup.h"
#include "state.h"
#include "usart.h"

#include <math.h>
#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

static void usb_show_intro(void);
static void usb_terminal_refresh(void);

static void report_reset_cause();
static void report_state();
static void periodic_report_state();

static void on_pushbutton_pressed();
static void handle_usart_rx_byte(uint8_t rx_byte);

static void (*reset)(void) = 0;

int main()
{
  setup();
  sei();

  usb_show_intro();
  report_reset_cause();

  for (;;) {
    if (interrupt_pushbutton) {
      on_pushbutton_pressed();
    }

    while (usart_rx_buf_has_data()) {
      const uint8_t rx_byte = usart_rx_buf_pop_byte();
      handle_usart_rx_byte(rx_byte);
    }

    state_update();

    switch (state.temperature_alert) {
      case ALERT_NONE:
      case ALERT_LOWER:
        if (state.dimming_factor < 1.0f) {
          state.dimming_factor = fminf(1.0f, state.dimming_factor + 0.0001f);
        }
        break;
      case ALERT_UPPER:
        if (state.dimming_factor > 0.3f) {
          state.dimming_factor = 0.3f;
        }
        else {
          state.dimming_factor *= 0.9999f;
        }
        break;
      case ALERT_CRITICAL:
        state.dimming_factor = 0.0f;
        // TODO: do more than just set the LEDs to minimal power.
        break;
    }

    state.pwm_white = light_visual_white(state.adc_white, state.dimming_factor);
    state.pwm_green = light_visual_green(state.adc_green, state.dimming_factor);

    led_aux(state.temperature_alert != ALERT_NONE);

    periodic_report_state();

    wdt_reset();

    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();
  }
}

static void report_state()
{
  const float vref = state.adc_vref * 3.3 / 1024.0;
  usart << R"({"adc": {"green": )" << state.adc_green;
  usart << R"(, "white": )" << state.adc_white;
  usart << R"(}, "pwm": {"green": )" << state.pwm_green;
  usart << R"(, "white": )" << state.pwm_white;
  usart << R"(}, "vref": )" << vref;
  usart << R"(, "temp": )" << state.temperature;
  if (state.dimming_factor != 1.0f) {
    usart << R"(, "dimming_factor": )" << state.dimming_factor;
  }
  if (state.temperature_alert) {
    usart << R"(, "temp_alert": ")";
    switch (state.temperature_alert) {
      case ALERT_NONE:
        break;
      case ALERT_LOWER:
        usart << "LOWER";
        break;
      case ALERT_UPPER:
        usart << "UPPER";
        break;
      case ALERT_CRITICAL:
        usart << "CRITICAL";
        break;
    }
    usart << "\"";
  }
  usart << "}\r\n";
}

static void periodic_report_state()
{
  static uint8_t last_time_mod100 = 0;
  const uint8_t time_mod100 = time_centiseconds % 100;

  if (time_mod100 == last_time_mod100) {
    return;
  }
  last_time_mod100 = time_mod100;

  if (time_mod100 == 0) {
    report_state();
  }
}

static void usb_terminal_refresh()
{
  usb_show_intro();
  report_state();
}

static void on_pushbutton_pressed()
{
  interrupt_pushbutton = false;

  bool last_pressed = false;
  const bool is_pressed = is_pushbutton_pressed();
  // led_aux(is_pressed);

  if (!last_pressed && is_pressed)
    usb_terminal_refresh();
  last_pressed = is_pressed;
}

static void handle_usart_rx_byte(uint8_t rx_byte)
{
  switch (rx_byte) {
    case 's':  // Status report.
      report_state();
      break;
    case 'r':  // Reboot.
      reset();
      break;
    case 'l':  // Lock up.
      usart << "Locking up\r\n";
      for (;;)
        ;
      break;
    case '/':
    case 'L' - 'A' + 1:  // Ctrl+L
      usb_terminal_refresh();
      break;
  }
}

static void usb_show_intro(void)
{
  // Source: https://www2.ccs.neu.edu/research/gpc/VonaUtils/vona/terminal/vtansi.htm
  usart << "\033[7l";    // Disable line wrap
  usart << "\033[r";     // Set scrolling region to entire screen.
  usart << "\033[2J";    // Clear screen
  usart << "\033[?25l";  // Hide cursor
  usart << "\r---------------------------\r\n";
  usart << " \033[95mSybren\033[0m's \033[92mORK LEDs v2\033[0m\r\n";
  usart << "---------------------------\r\n";
  usart << "r: \033[1mr\033[0meboot device\r\n";
  usart << "s: \033[1ms\033[0mtatus report\r\n";
  usart << "l: \033[1ml\033[0mock up the device\r\n";
  usart << "/: reset terminal\r\n";
  usart << "---------------------------\r\n";
  usart << "\r\n";

  usart << "\033[25;0H";  // Move cursor
  usart << "---------------------------\r\n";

  usart << "\033[10;24r";  // Set scrolling region
  usart << "\033[10;0H";   // Move cursor
}

static void report_reset_cause()
{
  usart << "Device booted from ";
  if (MCUSR & (1 << WDRF)) {
    usart << "watchdog";
  }
  else if (MCUSR & (1 << BORF)) {
    usart << "brown-out";
  }
  else if (MCUSR & (1 << EXTRF)) {
    usart << "external";
  }
  else if (MCUSR & (1 << PORF)) {
    usart << "power-on";
  }
  else {
    usart << "unknown";
  }
  usart << " reset\r\n";
  MCUSR &= ~((1 << WDRF) | (1 << BORF) | (1 << EXTRF) | (1 << PORF));
}
