/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "light.h"
#include "light_map.h"

#include <stdint.h>

#include <avr/io.h>

void light_pwm_white(const light_pwm_value_t pwm_value)
{
  OCR1A = pwm_value;
}
void light_pwm_green(const light_pwm_value_t pwm_value)
{
  OCR1B = pwm_value;
}

int16_t light_visual_white(const int16_t visual_intensity, const float dimming_factor)
{
  const float fractional_intensity = visual_intensity / 1023.0f;
  const light_pwm_value_t pwm_value = light_map_lookup(fractional_intensity * dimming_factor);
  light_pwm_white(pwm_value);
  return pwm_value;
}

int16_t light_visual_green(const int16_t visual_intensity, const float dimming_factor)
{
  const float fractional_intensity = visual_intensity / 1023.0f;
  const light_pwm_value_t pwm_value = light_map_lookup(fractional_intensity * dimming_factor);
  light_pwm_green(pwm_value);
  return pwm_value;
}

void light_setup()
{
// Timer 1 for LED PWM dimming.
#if LIGHT_BITS == 10
  // WGM 0b0111 for 10-bit fast PWM mode.
  constexpr uint8_t wgm_lowbits = 0b11;
#elif LIGHT_BITS == 8
  // WGM 0b0101 for 8-bit fast PWM mode.
  constexpr uint8_t wgm_lowbits = 0b01;
#endif

  TCCR1A = 0                       //
           | wgm_lowbits << WGM10  // 8-bit (01) or 10-bit (11) fast PWM mode
           | 0b10 << COM1A0        // normal PWM on OC1A / White
           | 0b10 << COM1B0        // normal PWM on OC1B / Green
      ;
  TCCR1B = 0                //
           | 0b01 << WGM12  // 8/10-bit fast PWM mode
           | 0b010 << CS00  // clkdiv/8
      ;
  OCR1A = 10;
  OCR1B = 10;
  TIFR1 = 0;   // Clear all interrupt flags
  TIMSK1 = 0;  // Disable interrupts
}
