/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "interrupts.h"

#include <avr/interrupt.h>
#include <util/atomic.h>

volatile bool interrupt_temperature_update = true;
volatile bool interrupt_pushbutton = false;
volatile uint32_t time_centiseconds = 0;

ISR(PCINT1_vect)
{
  interrupt_pushbutton = true;
}

ISR(TIMER3_COMPA_vect)
{
  time_centiseconds++;
}
