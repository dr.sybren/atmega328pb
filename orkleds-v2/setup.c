/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "setup.h"
#include "adc.h"
#include "hardware.h"
#include "light.h"
#include "mcp9808_driver.h"
#include "state.h"
#include "thermometer.h"
#include "usart.h"

#include <stdbool.h>

#include <avr/io.h>

#define MCP_I2C_ADDR 0x18

static void setup_timers();

void setup()
{
  // Enable watchdog.
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  WDTCSR = 0                 //
           | 1 << WDE        // Watchdog Enable
           | 0b0110 << WDP0  // 1s
      ;

  // Set the expected clockdiv.
  CLKPR = 1 << CLKPCE;
#if F_CPU == 8000000
  CLKPR = 0b0000;
#elif F_CPU == 1000000
  CLKPR = 0b0011;
#else
#  error Unexpected value for F_CPU
#endif

  PCICR = 0;  // Disable PCINT except the ones we configure.
  GTCCR = 0;  // Disable any timer sync at boot.

  DDRB = DDRC = DDRD = DDRE = 0;      // All pins as input by default.
  PORTB = PORTC = PORTD = PORTE = 0;  // Disable all pull-ups.
  PCMSK0 = PCMSK1 = PCMSK2 = 0;       // Disable all pin change interrupts.

  // The big LEDs are active low, so minimize the startup-blink caused by the default-low of the
  // output pins.
  PORTB |= (1 << PB1) | (1 << PB2);

  PRR0 = 0                  //
         | (1 << PRSPI0)    // SPI bus 0
         | (1 << PRUSART1)  // USART 1
      ;
  PRR1 = 0                //
         | (1 << PRSPI1)  // SPI bus 1
      ;

  // AUX LED on PD5.
  DDRD = 1 << DDD5;
  DDRB = 1 << DDB1 | 1 << DDB2;

  // Set PC0 / PCINT8 as pulled-up input for pushbutton:
  PORTC = 1 << PORTC0;
  PCMSK1 |= 1 << PCINT8;
  PCICR = 1 << PCIE1;

  // Set PD2 / INT0 as pulled-up input for temp alert:
  PORTD |= (1 << PORTD2);
  // TODO: enable interrupt.

  usart_setup();
  light_setup();
  thermometer_setup();
  setup_timers();
  adc_setup();
  state_setup();

  // Wait for all the pull-up resistors to do their work.
  while (is_pushbutton_pressed())
    ;
}

static void setup_timers()
{
  // Timer 3 for keeping track of time.
#if F_CPU != 8000000
#  error Timer 3 config assumes 8 MHz clock
#endif
  TCCR3A = 0b00 << WGM30;   // WGM = 0b0100 = CTC
  TCCR3B = 0                //
           | 0b01 << WGM32  //
           | 0b101 << CS30  // clock / 1024
      ;
  OCR3A = 78;  // ~100 Hz interrupts
  TIFR3 = 0;   // Clear all interrupt flags
  TIMSK3 = 1 << OCIE3A;
}
