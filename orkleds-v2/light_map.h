/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#pragma once

#ifndef __cplusplus
#  error "This is a C++ header file; it requires C++ to be compiled."
#endif

#include <stdint.h>

// 8 or 10
#define LIGHT_BITS 8

#if LIGHT_BITS == 10
using light_pwm_value_t = uint16_t;
#elif LIGHT_BITS == 8
using light_pwm_value_t = uint8_t;
#else
#  error "Unsupported number of bits"
#endif

/**
 * Calculate the PWM value for a linear input value between 0 and 1.
 */
light_pwm_value_t light_map_lookup(const float value /* range [0, 1] */);
