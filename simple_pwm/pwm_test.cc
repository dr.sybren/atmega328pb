#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

/* Pin-out:
 *
 * PE2: OUT LED
 */

static inline void pin_write_led(const bool pin_state)
{
  if (pin_state) {
    PORTE |= 1 << PORTE2;
  }
  else {
    PORTE &= ~(1 << PORTE2);
  }
}

int main()
{
  DDRE = (1 << DDRE2);  // OUT on PE2
  pin_write_led(false);

  // Timer 0 for blinking the LED
  // WGM = 0b011 for Fast PWM
  TCCR0A = 0                //
           | 0b11 << WGM00  // WGM[1:0]
      ;
  TCCR0B = 0                //
           | 0b0 << WGM02   // WGM[2]
           | 0b101 << CS00  // Clock Select
      ;
  TIMSK0 = 0              //
           | 1 << OCIE0A  // Output Compare interrupt
           | 1 << TOIE0   // Overflow interrupt
      ;
  GTCCR = 0;
  OCR0A = 16;

  sei();
  for (;;) {
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();
  }

  return 0;
}

ISR(TIMER0_COMPA_vect)
{
  pin_write_led(false);
}

ISR(TIMER0_OVF_vect)
{
  pin_write_led(true);
}
