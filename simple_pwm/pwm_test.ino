#include <Arduino.h>

#define LIGHT_PIN 9
#define TIMER1_MAX_VAL 255
#define POT_MAX_VAL 1023
#define BASE 256

void setup()
{
  Serial.begin(115200);
  pinMode(A6, INPUT);
  pinMode(LIGHT_PIN, OUTPUT);

  // Set up Timer 1 for controlling the light PWM on PB1 (OC1A aka pin 9).
  TCCR1A = (2<<COM1A0) | (2<<WGM10);
  TCCR1B = (3<<WGM12) | (1<<CS10);
  TCCR1C = 0;
  ICR1 = TIMER1_MAX_VAL;
}

void loop()
{
  float pot = analogRead(A6);
  float input = pot / POT_MAX_VAL;
  uint16_t pwm_value = (pow(BASE, input)-1)/(BASE-1) * TIMER1_MAX_VAL;
  Serial.println(pwm_value);

  OCR1A = pwm_value;
}
