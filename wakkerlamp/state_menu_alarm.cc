/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "state_menu_alarm.hh"
#include "alarm_time.hh"
#include "ds1302.hh"
#include "global_activity.hh"
#include "hardware.hh"
#include "light.hh"
#include "oled.hh"
#include "pushbuttons.hh"
#include "rotary_encoder.hh"
#include "segdisp.hh"
#include "state_menu_top.hh"

StateMenuAlarm state_menu_alarm;

void StateMenuAlarm::exit()
{
  alarm_time_store();
  light_set_smoothing(false);
  light_set_intensity_level(light_intensity_index);
  AbstractStateMenu::exit();
}

void StateMenuAlarm::draw()
{
  display.draw_rect(0, 16, SCREEN_WIDTH - 1, SCREEN_HEIGHT - 1);
  display.set_text_size(1);
  display.draw_text_centered(3, "SET ALARM");

  constexpr uint8_t char_width = 3 * 6;
  constexpr uint8_t text_width = 5 * char_width;
  constexpr uint8_t text_x = SCREEN_WIDTH / 2 - text_width / 2;
  constexpr uint8_t text_y = 22;
  display.set_text_size(3);
  display.set_text_position(text_x, text_y);
  display.draw_time(alarm_time.hour, alarm_time.minute);

  constexpr uint8_t line_width = 2 * char_width;
  constexpr uint8_t line_y = text_y + 26;
  display.draw_line(text_x,
                    line_y,
                    text_x + line_width,
                    line_y,
                    editing_minutes_ ? PixelColor::BLACK : PixelColor::WHITE);
  display.draw_line(text_x + 3 * char_width,
                    line_y,
                    text_x + 3 * char_width + line_width,
                    line_y,
                    editing_minutes_ ? PixelColor::WHITE : PixelColor::BLACK);
}

void StateMenuAlarm::on_rotate_left(const uint8_t clicks)
{
  if (editing_minutes_) {
    --alarm_time.minute;
    if (alarm_time.minute > 59)
      alarm_time.minute = 59;
  }
  else {
    --alarm_time.hour;
    if (alarm_time.hour > 23)
      alarm_time.hour = 23;
  }
}
void StateMenuAlarm::on_rotate_right(const uint8_t clicks)
{
  if (editing_minutes_) {
    ++alarm_time.minute;
    if (alarm_time.minute > 59)
      alarm_time.minute = 0;
  }
  else {
    ++alarm_time.hour;
    if (alarm_time.hour > 23)
      alarm_time.hour = 0;
  }
}
void StateMenuAlarm::on_click()
{
  editing_minutes_ = !editing_minutes_;
}
