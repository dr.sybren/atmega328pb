/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include "ds1302.hh"

#include <stdint.h>

struct AlarmTime {
  uint8_t hour;
  uint8_t minute;
  uint8_t days_of_week;  // A bit for each day of the week, LSB unused.
};

void alarm_time_setup();  // Load from EEPROM.
void alarm_time_store();  // Store into EEPROM.

bool alarm_time_is_now();
void alarm_time_turn_off();

extern AlarmTime alarm_time;
