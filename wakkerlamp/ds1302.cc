/* ATmega328pb Wakkerlamp */

// DS1302 RTC
// ----------
//
// Open Source / Public Domain
//
// Version 1
//     By arduino.cc user "Krodal".
//     June 2012
//     Using Arduino 1.0.1
// Version 2
//     By arduino.cc user "Krodal"
//     March 2013
//     Using Arduino 1.0.3, 1.5.2
//     The code is no longer compatible with older versions.
//     Added bcd2bin, bin2bcd_h, bin2bcd_l
//     A few minor changes.
// Version 3
//     By dr. Sybren A. Stüvel
//     November 2011
//     Converted to plain AVR code for Wakkerlamp

#include "ds1302.hh"
#include "timekeeping.hh"

#include <avr/io.h>
#include <stdbool.h>

// Register names.
// Since the highest bit is always '1',
// the registers start at 0x80
// If the register is read, the lowest bit should be '1'.
#define DS1302_SECONDS 0x80
#define DS1302_MINUTES 0x82
#define DS1302_HOURS 0x84
#define DS1302_DATE 0x86
#define DS1302_MONTH 0x88
#define DS1302_DAY 0x8A
#define DS1302_YEAR 0x8C
#define DS1302_ENABLE 0x8E
#define DS1302_TRICKLE 0x90
#define DS1302_CLOCK_BURST 0xBE
#define DS1302_CLOCK_BURST_WRITE 0xBE
#define DS1302_CLOCK_BURST_READ 0xBF
#define DS1302_RAMSTART 0xC0
#define DS1302_RAMEND 0xFC
#define DS1302_RAM_BURST 0xFE
#define DS1302_RAM_BURST_WRITE 0xFE
#define DS1302_RAM_BURST_READ 0xFF

// Bit for reading (bit in address)
#define DS1302_READBIT 0  // READBIT=1: read instruction

// Bit for clock (0) or ram (1) area,
// called R/C-bit (bit in address)
#define DS1302_RC 6

// Seconds Register
#define DS1302_CH 7  // 1 = Clock Halt, 0 = start

// Hour Register
#define DS1302_AM_PM 5  // 0 = AM, 1 = PM
#define DS1302_12_24 7  // 0 = 24 hour, 1 = 12 hour

// Enable Register
#define DS1302_WP 7  // 1 = Write Protect, 0 = enabled

// Trickle Register
#define DS1302_ROUT0 0
#define DS1302_ROUT1 1
#define DS1302_DS0 2
#define DS1302_DS1 2
#define DS1302_TCS0 4
#define DS1302_TCS1 5
#define DS1302_TCS2 6
#define DS1302_TCS3 7

void _DS1302_start(void);
void _DS1302_stop(void);
uint8_t _DS1302_toggleread(void);
void _DS1302_togglewrite(uint8_t data, bool release);

// --------------------------------------------------------
// DS1302_clock_burst_read
//
// This function reads 8 bytes clock data in burst mode
// from the DS1302.
//
// This function may be called as the first function,
// also the pinMode is set.
//
void DS1302_clock_burst_read(ds1302_struct *ds1302_data)
{
  uint8_t *p = reinterpret_cast<uint8_t *>(ds1302_data);

  _DS1302_start();

  // Instead of the address,
  // the CLOCK_BURST_READ command is issued
  // the I/O-line is released for the data
  _DS1302_togglewrite(DS1302_CLOCK_BURST_READ, true);
  for (uint8_t i = 0; i < 8; i++) {
    *p++ = _DS1302_toggleread();
  }
  _DS1302_stop();
}

// --------------------------------------------------------
// DS1302_clock_burst_write
//
// This function writes 8 bytes clock data in burst mode
// to the DS1302.
//
// This function may be called as the first function,
// also the pinMode is set.
//
void DS1302_clock_burst_write(const ds1302_struct *ds1302_data)
{
  const uint8_t *p = reinterpret_cast<const uint8_t *>(ds1302_data);

  _DS1302_start();

  // Instead of the address,
  // the CLOCK_BURST_WRITE command is issued.
  // the I/O-line is not released
  _DS1302_togglewrite(DS1302_CLOCK_BURST_WRITE, false);

  for (uint8_t i = 0; i < 8; i++) {
    // the I/O-line is not released
    _DS1302_togglewrite(*p++, false);
  }
  _DS1302_stop();
}

// --------------------------------------------------------
// DS1302_read
//
// This function reads a byte from the DS1302
// (clock or ram).
//
// The address could be like "0x80" or "0x81",
// the lowest bit is set anyway.
//
// This function may be called as the first function,
// also the pinMode is set.
//
uint8_t DS1302_read(int address)
{
  uint8_t data;

  // set lowest bit (read bit) in address
  address |= 1 << DS1302_READBIT;

  _DS1302_start();
  // the I/O-line is released for the data
  _DS1302_togglewrite(address, true);
  data = _DS1302_toggleread();
  _DS1302_stop();

  return (data);
}

// --------------------------------------------------------
// DS1302_write
//
// This function writes a byte to the DS1302 (clock or ram).
//
// The address could be like "0x80" or "0x81",
// the lowest bit is cleared anyway.
//
// This function may be called as the first function,
// also the pinMode is set.
//
void DS1302_write(int address, uint8_t data)
{
  // clear lowest bit (read bit) in address
  address &= ~(1 << DS1302_READBIT);

  _DS1302_start();
  // don't release the I/O-line
  _DS1302_togglewrite(address, false);
  // don't release the I/O-line
  _DS1302_togglewrite(data, false);
  _DS1302_stop();
}

// --------------------------------------------------------
// _DS1302_start
//
// A helper function to setup the start condition.
//
// An 'init' function is not used.
// But now the pinMode is set every time.
// That's not a big deal, and it's valid.
// At startup, the pins of the Arduino are high impedance.
// Since the DS1302 has pull-down resistors,
// the signals are low (inactive) until the DS1302 is used.
void _DS1302_start(void)
{
  PORTC &= ~DS1302_PINS_MASK;  // Pull pins low.
  DDRC |= DS1302_PINS_MASK;    // Configure as output.
  timekeeping_delay_usec(100);

  PORTC |= DS1302_CE_PIN_MASK;  // Start the session by pulling CE high.
  timekeeping_delay_usec(4);    // tCC = 4us
}

// --------------------------------------------------------
// _DS1302_stop
//
// A helper function to finish the communication.
//
void _DS1302_stop(void)
{
  PORTC &= ~DS1302_CE_PIN_MASK;  // Stop the session by pulling CE low.
  timekeeping_delay_usec(4);     // tCWH = 4us
}

__attribute__((always_inline)) inline void _DS1302_clock_high()
{
  PORTC |= DS1302_SCLK_PIN_MASK;
}
__attribute__((always_inline)) inline void _DS1302_clock_low()
{
  PORTC &= ~DS1302_SCLK_PIN_MASK;
}

// --------------------------------------------------------
// _DS1302_toggleread
//
// A helper function for reading a byte with bit toggle
//
// This function assumes that the SCLK is still high.
//
uint8_t _DS1302_toggleread(void)
{
  uint8_t data = 0;
  for (uint8_t i = 0; i < 8; i++) {
    // Issue a clock pulse for the next databit.
    // If the 'togglewrite' function was used before
    // this function, the SCLK is already high.
    _DS1302_clock_high();
    timekeeping_delay_usec(1);

    // Clock down, data is ready after some time.
    _DS1302_clock_low();
    timekeeping_delay_usec(1);  // tCL=1000ns, tCDD=800ns

    // read bit, and set it in place in 'data' variable
    const bool data_bit = (PINC & DS1302_DATA_PIN_MASK) != 0;
    data |= data_bit << i;
  }

  return data;
}

// --------------------------------------------------------
// _DS1302_togglewrite
//
// A helper function for writing a byte with bit toggle
//
// The 'release' parameter is for a read after this write.
// It will release the I/O-line and will keep the SCLK high.
//
void _DS1302_togglewrite(uint8_t data, bool release)
{

  for (uint8_t i = 0; i <= 7; i++) {
    // Set a bit of the data on the I/O-line.
    if (data & 1) {
      PORTC |= DS1302_DATA_PIN_MASK;
    }
    else {
      PORTC &= ~DS1302_DATA_PIN_MASK;
    }
    data >>= 1;
    timekeeping_delay_usec(1);  // tDC = 200ns

    // clock up, data is read by DS1302
    _DS1302_clock_high();
    timekeeping_delay_usec(1);  // tCH = 1000ns, tCDH = 800ns

    if (release && i == 7) {
      // If this write is followed by a read,
      // the I/O-line should be released after
      // the last bit, before the clock line is made low.
      // This is according the datasheet.
      // I have seen other programs that don't release
      // the I/O-line at this moment,
      // and that could cause a shortcut spike
      // on the I/O-line.
      DDRC &= ~DS1302_DATA_PIN_MASK;
      PORTC &= ~DS1302_DATA_PIN_MASK;
    }
    else {
      _DS1302_clock_low();
      timekeeping_delay_usec(1);  // tCL=1000ns, tCDD=800ns
    }
  }
}
