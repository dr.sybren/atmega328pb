/* Wakkerlamp: pushbuttons
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "pushbuttons.hh"
#include "global_activity.hh"

// Pinout:
//   Button RIGHT: PC1 PCINT9
//   Button LEFT : PC2 PCINT10

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/atomic.h>

constexpr uint8_t PBUTS_LEFT_PIN_MASK = (1 << PORTC2);
constexpr uint8_t PBUTS_RIGHT_PIN_MASK = (1 << PORTC1);
constexpr uint8_t PBUTS_ALL_PIN_MASK = PBUTS_LEFT_PIN_MASK | PBUTS_RIGHT_PIN_MASK;

volatile uint8_t pbuts_left_clicks;
volatile uint8_t pbuts_right_clicks;

void pushbuttons_setup()
{
  // Set pins to input with pull-up resistors.
  DDRC &= ~PBUTS_ALL_PIN_MASK;
  PORTC |= PBUTS_ALL_PIN_MASK;

  // Wait for pull-up resistors to do their work.
  while ((PORTC & PBUTS_ALL_PIN_MASK) != PBUTS_ALL_PIN_MASK)
    ;

  for (uint16_t delay = 0; delay < 3000; ++delay)
    __asm__ __volatile__("nop");

  // Configure pin change interrupts.
  PCICR |= 1 << PCIE1;
  PCMSK1 |= 0               //
            | 1 << PCINT9   // Right: PC1
            | 1 << PCINT10  // Left: PC2
      ;

  // Clear any interrupt flag that was set due to the pins being high.
  PCIFR = 0;
}

void pushbuttons_reset()
{
  pbuts_left_clicks = 0;
  pbuts_right_clicks = 0;
}

uint8_t pushbuttons_get_clicks_and_reset_left()
{
  uint8_t clicks;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    clicks = pbuts_left_clicks;
    pbuts_left_clicks = 0;
  }
  return clicks;
}

uint8_t pushbuttons_get_clicks_and_reset_right()
{
  uint8_t clicks;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    clicks = pbuts_right_clicks;
    pbuts_right_clicks = 0;
  }
  return clicks;
}

void on_left_click()
{
  if (pbuts_left_clicks < 100)
    ++pbuts_left_clicks;
}

void on_right_click()
{
  if (pbuts_right_clicks < 100)
    ++pbuts_right_clicks;
}

ISR(PCINT1_vect)
{
  static time_csec_t last_click = 0;
  const time_csec_t now = timekeeping_centisec();
  if (now - last_click < 25)
    return;
  last_click = now;

  const uint8_t pinC = PINC;
  if ((pinC & PBUTS_LEFT_PIN_MASK) == 0) {
    on_left_click();
    return;
  }
  if ((pinC & PBUTS_RIGHT_PIN_MASK) == 0) {
    on_right_click();
    return;
  }
}
