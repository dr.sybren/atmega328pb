/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "light.hh"
#include "algorithm.hh"
#include "hardware.hh"
#include "ssd1306.hh"
#include "timekeeping.hh"

#include <math.h>

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/atomic.h>

volatile uint8_t light_intensity_index = 0;

namespace {
const uint16_t LIGHT_PWM_MAX = 0x03FF;

volatile uint16_t target_pwm_value = 0;  // OCR1B is gradually brought to this level.
volatile bool is_smoothing = false;

// "visual intensity" is the intensity level normalized to [0.0, 1.0], where 0.0 is "off" and 1.0
// is "brightest".

float intensity_index_to_visual_intensity(const uint8_t light_intensity_index)
{
  return (light_intensity_index + 1) / (float)LIGHT_INTENSITY_LEVELS;
}
float intensity_index_to_visual_intensity(const float light_intensity_index)
{
  return (light_intensity_index + 1) / (float)LIGHT_INTENSITY_LEVELS;
}
float interpolated_intensity_index(const uint16_t current_pwm)
{
  for (uint8_t level_index = 0; level_index < LIGHT_INTENSITY_LEVELS; ++level_index) {
    const uint16_t pwm_for_level = pgm_read_word(&light_map_10bit[level_index]);

    if (pwm_for_level < current_pwm)
      continue;

    if (current_pwm == pwm_for_level)
      return level_index;

    // The light map starts at 0 with increments of 1, so if the above condition
    // is false, it is certain that level_index > 1.
    const uint8_t prev_level_index = level_index - 1;
    const uint16_t pwm_for_prev = pgm_read_word(&light_map_10bit[prev_level_index]);
    const float pwm_diff = pwm_for_level - pwm_for_prev;
    const float level_fraction = (current_pwm - pwm_for_prev) / pwm_diff;
    const float level = prev_level_index + level_fraction;

    return level;
  }
  return LIGHT_INTENSITY_LEVELS - 1;
}
float interpolated_visual_intensity()
{
  const float intensity_index = interpolated_intensity_index(OCR1B);
  return intensity_index_to_visual_intensity(intensity_index);
}

void light_disable_pwm()
{
  TCCR1A = 0;
  TCCR1B = 0;
  PORTB &= ~(1 << PORTB2);  // turn off LED output
}

void light_enable_pwm()
{
  // Set up Timer 1 for controlling the light PWM on PB2 (OC1B).
  // WGM = 0b0111 for fast 10-bit PWM.
  TCCR1A = 0 | (0b10 << COM1B0) | (0b11 << WGM10);
  TCCR1B = (0b01 << WGM12) | (0b001 << CS10);
  TCCR1C = 0;

  if (is_smoothing) {
    TIMSK1 = 1 << TOIE1;
  }
  else {
    TIMSK1 = 0;
  }

  TCNT1 = 0;  // Counter Value
}
}  // namespace

void light_setup()
{
  DDRB |= (1 << DDB2);  // Set PB2 as output
  light_off();
}

void light_on()
{
  light_enable_pwm();
}

void light_off()
{
  light_disable_pwm();
}

bool light_is_on()
{
  return (TCCR1A != 0);
}

void light_toggle()
{
  if (light_is_on())
    light_off();
  else
    light_on();
}

void light_blink()
{
  uint16_t old_ocr1b = OCR1B;
  OCR1B = 0;
  for (volatile uint16_t timer = 0; timer < 40000; ++timer)
    __asm__("nop");
  OCR1B = LIGHT_PWM_MAX >> 4;
  for (volatile uint16_t timer = 0; timer < 20000; ++timer)
    __asm__("nop");
  OCR1B = 0;
  for (volatile uint16_t timer = 0; timer < 20000; ++timer)
    __asm__("nop");
  OCR1B = old_ocr1b;
}

ISR(TIMER1_OVF_vect)
{
  if (OCR1B == target_pwm_value)
    return;

  static time_csec_t last_update = 0;
  const time_csec_t now_csec = timekeeping_centisec();
  if (now_csec - last_update < 10)
    return;
  last_update = now_csec;

  const uint16_t step_size = max(1U, (OCR1B >> 6));
  const uint16_t target_value = target_pwm_value;
  if (OCR1B < target_value) {
    const uint16_t new_value = OCR1B + step_size;
    OCR1B = min(new_value, target_value);
  }
  else if (OCR1B > target_value) {
    if (OCR1B < step_size)
      OCR1B = 0;
    else
      OCR1B -= step_size;
  }
}

void light_set_pwm(uint16_t pwm_value)
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    if (is_smoothing)
      target_pwm_value = pwm_value;
    else
      OCR1B = pwm_value;
  }

  if (TCCR1A == 0)
    light_enable_pwm();
}

void light_set_intensity_level(uint8_t level)
{
  const uint16_t pwm_value = pgm_read_word(&light_map_10bit[level]);
  light_set_pwm(pwm_value);
  light_intensity_index = level;
}

float light_get_visual_intensity()
{
  if (TCCR1A == 0)
    return 0.0f;
  return interpolated_visual_intensity();
}

void light_brighter()
{
  if (TCCR1A == 0) {
    light_set_intensity_level(0);
    return;
  }
  if (light_intensity_index + 1 == LIGHT_INTENSITY_LEVELS)
    return;
  light_set_intensity_level(++light_intensity_index);
}

void light_darker()
{
  if (light_intensity_index == 0)
    return;
  light_set_intensity_level(--light_intensity_index);
}

void light_set_smoothing(const bool new_is_smoothing)
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    if (new_is_smoothing) {
      TIMSK1 |= (1 << TOIE1);
    }
    else {
      TIMSK1 &= ~(1 << TOIE1);
      OCR1B = target_pwm_value;
    }
    is_smoothing = new_is_smoothing;
  }
}

void light_debug_draw()
{
  const uint16_t current_pwm = OCR1B;
  const float intensity = interpolated_visual_intensity();
  display.draw_progress_bar(intensity);
  display.draw_progress_bar(current_pwm / 1023.0f, 16);

  display.set_text_size(1);
  display.set_text_position(3, 32);
  display.draw_text("TGT=");
  display.draw_uint(target_pwm_value);
  display.set_text_position(3, 40);
  display.draw_text("PWM=");
  display.draw_uint(current_pwm);
  display.set_text_position(3, 48);
  display.draw_text("INT=");
  display.draw_float(intensity);
  display.display();
}
