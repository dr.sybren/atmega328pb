/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "alarm_time.hh"
#include "clock.hh"
#include "eeprom_offsets.hh"

#include <avr/eeprom.h>

AlarmTime alarm_time;

namespace {
volatile bool was_turned_off = false;
}

void alarm_time_setup()
{
  alarm_time.hour = eeprom_read_byte(EEPROM_ALARM_TIME_HOURS);
  alarm_time.minute = eeprom_read_byte(EEPROM_ALARM_TIME_MINUTES);
  alarm_time.days_of_week = eeprom_read_byte(EEPROM_ALARM_TIME_DOW);

  if (alarm_time.hour < 24 && alarm_time.minute < 60) {
    // Time is trustworthy
    return;
  }

  // Set up some ok-ish alarm time.
  alarm_time.hour = 23;
  alarm_time.minute = 30;
}

void alarm_time_store()
{
  eeprom_update_byte(EEPROM_ALARM_TIME_HOURS, alarm_time.hour);
  eeprom_update_byte(EEPROM_ALARM_TIME_MINUTES, alarm_time.minute);
  eeprom_update_byte(EEPROM_ALARM_TIME_DOW, alarm_time.days_of_week);
}

bool alarm_time_is_now()
{
  const bool alarm_is_now = bcd2bin(now.h24.Hour10, now.h24.Hour) == alarm_time.hour &&
                            bcd2bin(now.Minutes10, now.Minutes) == alarm_time.minute &&
                            (alarm_time.days_of_week & (1 << now.DayOfWeek)) != 0;
  was_turned_off &= alarm_is_now;
  return alarm_is_now && !was_turned_off;
}

void alarm_time_turn_off()
{
  was_turned_off = true;
}
