/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "state_menu_top.hh"
#include "ds1302.hh"
#include "global_activity.hh"
#include "hardware.hh"
#include "light.hh"
#include "oled.hh"
#include "pushbuttons.hh"
#include "rotary_encoder.hh"
#include "segdisp.hh"
#include "state_idle.hh"

StateMenuTop state_menu_top;

namespace {
inline uint8_t choice_index_to_pixel_x(const uint8_t choice_index)
{
  return 16 + choice_index * 20;
}
}  // namespace

void StateMenuTop::draw()
{
  display.clear();
  display.draw_rect(0, 0, SCREEN_WIDTH - 1, 15);

  const uint8_t text_top_y = 3;
  const uint8_t pixel_x = choice_index_to_pixel_x(current_choice_index_);
  uint8_t choice_index = 0;
  display.set_text_size(1);
  for (const Choice &choice : choices_) {
    display.set_text_position(choice_index_to_pixel_x(choice_index), text_top_y);
    display.draw_character(choice.letter);
    ++choice_index;
  }
  display.draw_rect_filled(pixel_x - 5, 1, pixel_x + 8, 14, PixelColor::INVERT);

  display.draw_text_centered(32, choices_[current_choice_index_].description);
}

void StateMenuTop::on_rotate_left(const uint8_t clicks)
{
  if (current_choice_index_ > 0)
    --current_choice_index_;
}
void StateMenuTop::on_rotate_right(const uint8_t clicks)
{
  if (current_choice_index_ < num_choices_ - 1)
    ++current_choice_index_;
}
void StateMenuTop::on_click()
{
  AbstractState *const choice_state = choices_[current_choice_index_].state;
  state_machine.go_to_state(choice_state);
}
void StateMenuTop::on_back()
{
  state_machine.go_to_state(&state_idle);
}
