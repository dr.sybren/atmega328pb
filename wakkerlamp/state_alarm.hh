/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include "statemachine.hh"
#include "timekeeping.hh"

class StateAlarm : public AbstractState {
 private:
  time_csec_t alarm_start_time_;
  bool alarm_ended_due_to_timeout_;

 public:
  void enter() override;
  void exit() override;
  void tick() override;

 private:
  void update_light_brightness();
  void maybe_end_alarm();
  void end_alarm();
  void show_clock();
  void check_alarm_duration();
};

extern StateAlarm state_alarm;
