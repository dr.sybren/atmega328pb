/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "state_menu_abstract.hh"
#include "clock.hh"
#include "ds1302.hh"
#include "hardware.hh"
#include "pushbuttons.hh"
#include "rotary_encoder.hh"
#include "segdisp.hh"
#include "state_idle.hh"
#include "state_menu_top.hh"
#include "timekeeping.hh"

namespace {
void on_knob_changed(float knob_position);
}  // namespace

void AbstractStateMenu::enter()
{
  rotenc_reset();
  pushbuttons_reset();
  knob.set_callback(on_knob_changed);

  display.clear();
  display.power_on();
  display.set_contrast(display.DEFAULT_CONTRAST);
}

void AbstractStateMenu::exit()
{
  knob.set_callback(nullptr);
  display.clear();
}

void AbstractStateMenu::tick()
{
  knob.update();
  if (clock_update()) {
    segdisp_show_time(now);
  }

  if (pushbuttons_get_clicks_and_reset_left()) {
    state_machine.go_to_state(&state_idle);
  }
  if (pushbuttons_get_clicks_and_reset_right()) {
    on_back();
  }
  if (rotenc_get_clicks_and_reset()) {
    on_click();
  }

  const int8_t rots = rotenc_get_turns_and_reset();
  if (rots < 0) {
    on_rotate_left(-rots);
  }
  else if (rots > 0) {
    on_rotate_right(rots);
  }

  draw();
  display.display();
}

void AbstractStateMenu::draw()
{
}
void AbstractStateMenu::on_rotate_left(const uint8_t clicks)
{
}
void AbstractStateMenu::on_rotate_right(const uint8_t clicks)
{
}
void AbstractStateMenu::on_click()
{
}
void AbstractStateMenu::on_back()
{
  state_machine.go_to_state(&state_menu_top);
}

namespace {

void on_knob_changed(const float knob_position)
{
  const float intensity = knob_position * knob_position;
  segdisp_set_brightness(intensity * 255);
}

}  // namespace
