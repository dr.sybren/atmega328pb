/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "state_menu_time.hh"
#include "clock.hh"
#include "ds1302.hh"
#include "hardware.hh"
#include "oled.hh"
#include "pushbuttons.hh"
#include "rotary_encoder.hh"
#include "segdisp.hh"
#include "state_menu_top.hh"

#include <stdlib.h>

StateMenuTime state_menu_time;

void StateMenuTime::enter()
{
  clock_update();
  hours_ = bcd2bin(now.h24.Hour10, now.h24.Hour);
  minutes_ = bcd2bin(now.Minutes10, now.Minutes);
  AbstractStateMenu::enter();
}

void StateMenuTime::on_back()
{
  ds1302_struct new_time = now;

  const div_t hours = div(hours_, 10);
  new_time.h24.Hour10 = hours.quot;
  new_time.h24.Hour = hours.rem;

  const div_t minutes = div(minutes_, 10);
  new_time.Minutes10 = minutes.quot;
  new_time.Minutes = minutes.rem;

  new_time.Seconds10 = 0;
  new_time.Seconds = 0;

  DS1302_clock_burst_write(&new_time);
}

void StateMenuTime::draw()
{
  display.draw_rect(0, 16, SCREEN_WIDTH - 1, SCREEN_HEIGHT - 1);
  display.set_text_size(1);
  display.draw_text_centered(3, "SET TIME");

  constexpr uint8_t char_width = 3 * 6;
  constexpr uint8_t text_width = 5 * char_width;
  constexpr uint8_t text_x = SCREEN_WIDTH / 2 - text_width / 2;
  constexpr uint8_t text_y = 22;
  display.set_text_size(3);
  display.set_text_position(text_x, text_y);
  display.draw_time(hours_, minutes_);

  constexpr uint8_t line_width = 2 * char_width;
  constexpr uint8_t line_y = text_y + 26;
  display.draw_line(text_x,
                    line_y,
                    text_x + line_width,
                    line_y,
                    editing_minutes_ ? PixelColor::BLACK : PixelColor::WHITE);
  display.draw_line(text_x + 3 * char_width,
                    line_y,
                    text_x + 3 * char_width + line_width,
                    line_y,
                    editing_minutes_ ? PixelColor::WHITE : PixelColor::BLACK);
}

void StateMenuTime::on_rotate_left(const uint8_t clicks)
{
  if (editing_minutes_) {
    --minutes_;
    if (minutes_ < 0)
      minutes_ = 59;
  }
  else {
    --hours_;
    if (hours_ < 0)
      hours_ = 23;
  }
}
void StateMenuTime::on_rotate_right(const uint8_t clicks)
{
  if (editing_minutes_) {
    ++minutes_;
    if (minutes_ > 59)
      minutes_ = 0;
  }
  else {
    ++hours_;
    if (hours_ > 23)
      hours_ = 0;
  }
}
void StateMenuTime::on_click()
{
  editing_minutes_ = !editing_minutes_;
}
