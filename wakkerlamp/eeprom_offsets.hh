/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <stdint.h>

uint8_t *const EEPROM_ALARM_TIME_HOURS = 0;
uint8_t *const EEPROM_ALARM_TIME_MINUTES = reinterpret_cast<uint8_t *>(1);
uint8_t *const EEPROM_ALARM_TIME_DOW = reinterpret_cast<uint8_t *>(2);
