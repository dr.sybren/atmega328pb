/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "state_menu_dow.hh"
#include "alarm_time.hh"
#include "clock.hh"
#include "hardware.hh"
#include "state_menu_top.hh"

StateMenuDayOfWeek state_menu_dow;

namespace {
const char *const days_of_week[] = {
    "MO",
    "TU",
    "WE",
    "TH",
    "FR",
    "SA",
    "SU",
};
constexpr uint8_t text_size = 1;
constexpr uint8_t char_width = text_size * 6;
constexpr uint8_t text_width = 2 * char_width;
constexpr uint8_t field_width = text_width + char_width;
constexpr uint8_t total_width = 7 * field_width - char_width;
constexpr uint8_t text_x = SCREEN_WIDTH / 2 - total_width / 2;
constexpr uint8_t text_y = 32;
constexpr uint8_t line_y = text_y + text_size * 8 + 2;
constexpr uint8_t cursor_y = text_y - 3;
}  // namespace

void StateMenuDayOfWeek::enter()
{
  AbstractStateMenu::enter();

  display.draw_rect(0, 16, SCREEN_WIDTH - 1, SCREEN_HEIGHT - 1);
  display.set_text_size(1);
  display.draw_text_centered(3, "DAY OF WEEK");

  display.set_text_size(text_size);

  for (uint8_t field = 0; field < 7; ++field) {
    display.set_text_position(text_x + field * field_width, text_y);
    display.draw_text(days_of_week[field]);
  }

  set_field_index(now.DayOfWeek - 1);
}

void StateMenuDayOfWeek::exit()
{
  alarm_time_store();
  AbstractStateMenu::exit();
}

void StateMenuDayOfWeek::draw()
{
  for (uint8_t field = 0; field < 7; field++) {
    const uint8_t line_x = text_x + field * field_width;
    const bool enabled = alarm_time.days_of_week & (1 << (field + 1));
    const PixelColor color = enabled ? PixelColor::WHITE : PixelColor::BLACK;
    display.draw_line(line_x, line_y, line_x + text_width, line_y, color);
  }
}

void StateMenuDayOfWeek::on_rotate_left(const uint8_t clicks)
{
  set_field_index(field_index_ - 1);
}
void StateMenuDayOfWeek::on_rotate_right(const uint8_t clicks)
{
  set_field_index(field_index_ + 1);
}

void StateMenuDayOfWeek::set_field_index(int8_t field_index)
{
  draw_cursor_line(PixelColor::BLACK);
  field_index_ = (field_index + 7) % 7;
  draw_cursor_line(PixelColor::WHITE);
}

void StateMenuDayOfWeek::draw_cursor_line(PixelColor pixel_color)
{
  const uint8_t line_x = text_x + field_index_ * field_width;
  display.draw_line(line_x, cursor_y, line_x + text_width, cursor_y, pixel_color);
}

void StateMenuDayOfWeek::on_click()
{
  const uint8_t field_mask = 1 << (field_index_ + 1);
  alarm_time.days_of_week ^= field_mask;
}
