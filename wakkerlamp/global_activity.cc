/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "global_activity.hh"

#include <util/atomic.h>

volatile time_csec_t last_activity_csec = 0;

void global_activity_mark()
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    last_activity_csec = timekeeping_centisec();
  }
}

time_csec_t global_activity_time_since()
{
  const time_csec_t centisec = timekeeping_centisec();
  return centisec - last_activity_csec;
}
