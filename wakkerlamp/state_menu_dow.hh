/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include "ssd1306.hh"
#include "state_menu_abstract.hh"

class StateMenuDayOfWeek : public AbstractStateMenu {
 public:
  void enter();
  void exit();

 protected:
  int8_t field_index_;

  void draw() override;
  void on_rotate_left(uint8_t clicks) override;
  void on_rotate_right(uint8_t clicks) override;
  void on_click() override;

  void set_field_index(int8_t field_index);
  void draw_cursor_line(PixelColor pixel_color);
};
extern StateMenuDayOfWeek state_menu_dow;
