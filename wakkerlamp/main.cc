/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "alarm_time.hh"
#include "hardware.hh"
#include "light.hh"
#include "pushbuttons.hh"
#include "rotary_encoder.hh"
#include "segdisp.hh"
#include "statemachine.hh"
#include "timekeeping.hh"

#if START_WITH_ALARM
#  include "state_alarm.hh"
#else
#  include "state_idle.hh"
#endif

#include <stdlib.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

OLED display;
Knob knob;

namespace {
void setup();    // Set up pins & other hardware.
void startup();  // Show startup screen.
void loop();     // Main loops.
}  // namespace

int main()
{
  setup();
  sei();

  startup();

  for (;;)
    loop();
  return 0;
}

void *operator new(size_t n)
{
  return malloc(n);
}

void operator delete(void *p, size_t)
{
  free(p);
}

namespace {

void setup()
{
  // Set all pins to pulled-up inputs so that unused pins don't cause noise.
  DDRB = DDRC = DDRD = DDRE = 0;
  PORTB = PORTC = PORTD = 255;

  // Enable the Segment display 'disable output' pin
  PORTD |= 1 << PD5;

  light_setup();
  timekeeping_setup();
  alarm_time_setup();

  segdisp_setup();
  segdisp_set_decpoint(2);
  segdisp_show_time(alarm_time.hour, alarm_time.minute);

  knob.setup();
  display.init();
  rotary_encoder_setup();
  pushbuttons_setup();
}

void startup()
{
  light_on();
  light_set_pwm(0.001);

  display.clear();
  display.draw_text_centered(4, "dr. Sybren's");
  display.set_text_size(2);
  display.draw_text_centered(28, "Wakkerlamp");
  display.draw_border();
  display.display();

  // Give RTC time to start up.
  timekeeping_delay_csec(150);
  light_off();

#if START_WITH_ALARM
  state_machine.go_to_state(&state_alarm);
#else
  state_machine.go_to_state(&state_idle);
#endif
}

void loop()
{
  state_machine.tick();

  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_mode();
}

}  // namespace

extern "C" void __cxa_pure_virtual() { while (true); }
