/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

// Uses Timer 0 channel B for updates.
// Assumes timekeeping is already set up before knob.setup() is called.
// Reads from ADC3 = PC3

class Knob {
 public:
  typedef void (*callback)(float knob_value);

  void setup();
  bool update();
  void reset();
  float position() const;
  void set_callback(callback on_knob_value_changed);

 private:
  float last_position_ = -100.0f;
  float smoothed_position_ = 0.0f;
  callback on_value_changed_;

  float get_abs_position();
  float get_smooth_position();
};
