#include "rtc.h"

#include <Arduino.h>
#include <avr/sleep.h>

void setup()
{
  Serial.begin(115200);

  DS1302_clock_set(2019, 7, 24, 21, 48, 0, dowWednesday);
  Serial.println("Clock set to startup timestamp");

  DS1302_clock_refresh();
  Serial.println("Setup complete");
}

void loop()
{
  DS1302_clock_refresh();

  Serial.print(current_hour);
  Serial.print(":");
  Serial.print(current_minute);
  Serial.print(":");
  Serial.print(current_second);
  Serial.println("");

  delay(250);
}
