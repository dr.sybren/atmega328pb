#include "rtc.h"
#include "DS1302.h"

#include <string.h>
#include <util/atomic.h>

ds1302_struct current_rtc;

volatile uint8_t current_hour, current_minute, current_second;
volatile DayOfWeek current_dow;

void DS1302_clock_read(ds1302_struct *rtc)
{
  DS1302_clock_burst_read((uint8_t *)rtc);
}

void DS1302_clock_refresh()
{
  DS1302_clock_read(&current_rtc);

  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    current_hour = bcd2bin(current_rtc.h24.Hour10, current_rtc.h24.Hour);
    current_minute = bcd2bin(current_rtc.Minutes10, current_rtc.Minutes);
    current_second = bcd2bin(current_rtc.Seconds10, current_rtc.Seconds);
    current_dow = static_cast<DayOfWeek>(current_rtc.Day);
  }
}

void DS1302_clock_set(const ds1302_struct *rtc)
{
  DS1302_clock_burst_write((uint8_t *)rtc);
}

void DS1302_clock_set(uint16_t year,
                      uint8_t month,
                      uint8_t day,
                      uint8_t hour,
                      uint8_t minute,
                      uint8_t second,
                      DayOfWeek day_of_week)
{
  ds1302_struct rtc;
  memset(&rtc, 0, sizeof(rtc));

  rtc.Seconds = bin2bcd_l(second);
  rtc.Seconds10 = bin2bcd_h(second);
  rtc.Minutes = bin2bcd_l(minute);
  rtc.Minutes10 = bin2bcd_h(minute);
  rtc.h24.Hour = bin2bcd_l(hour);
  rtc.h24.Hour10 = bin2bcd_h(hour);
  rtc.Date = bin2bcd_l(day);
  rtc.Date10 = bin2bcd_h(day);
  rtc.Month = bin2bcd_l(month);
  rtc.Month10 = bin2bcd_h(month);
  rtc.Day = (uint8_t)day_of_week;

  uint8_t write_year = year - 2000;
  rtc.Year = bin2bcd_l(write_year);
  rtc.Year10 = bin2bcd_h(write_year);

  DS1302_clock_set(&rtc);
}

void fix_time(volatile uint8_t *hours, volatile uint8_t *minutes, volatile uint8_t *seconds)
{
  *minutes += *seconds / 60;
  *hours += *minutes / 60;
  *seconds %= 60;
  *minutes %= 60;
  *hours %= 24;
}
