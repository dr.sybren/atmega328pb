#pragma once

#include "DS1302.h"

extern ds1302_struct current_rtc;

enum DayOfWeek {
  dowMonday = 1,
  dowTuesday,
  dowWednesday,
  dowThursday,
  dowFriday,
  dowSaturday,
  dowSunday
};

extern volatile uint8_t current_hour, current_minute, current_second;
extern volatile DayOfWeek current_dow;

void DS1302_clock_refresh();
void DS1302_clock_read(ds1302_struct *rtc);
void DS1302_clock_set(const ds1302_struct *rtc);
void DS1302_clock_set(uint16_t year,
                      uint8_t month,
                      uint8_t day,
                      uint8_t hour,
                      uint8_t minute,
                      uint8_t second,
                      DayOfWeek day_of_week);

void fix_time(volatile uint8_t *hours, volatile uint8_t *minutes, volatile uint8_t *seconds);
