#pragma once

#include <stdint.h>

// Set your own pins with these defines !
#define DS1302_SCLK_PIN 6 // Arduino pin for the Serial Clock
#define DS1302_IO_PIN 7   // Arduino pin for the Data I/O
#define DS1302_CE_PIN 8   // Arduino pin for the Chip Enable

// Macros to convert the bcd values of the registers to normal
// integer variables.
// The code uses separate variables for the high byte and the low byte
// of the bcd, so these macros handle both bytes separately.
#define bcd2bin(h, l) (((h)*10) + (l))
#define bin2bcd_h(x) ((x) / 10)
#define bin2bcd_l(x) ((x) % 10)

// Structure for the first 8 registers.
// These 8 bytes can be read at once with
// the 'clock burst' command.
// Note that this structure contains an anonymous union.
// It might cause a problem on other compilers.
struct ds1302_struct
{
    uint8_t Seconds : 4;   // low decimal digit 0-9
    uint8_t Seconds10 : 3; // high decimal digit 0-5
    uint8_t CH : 1;        // CH = Clock Halt
    uint8_t Minutes : 4;
    uint8_t Minutes10 : 3;
    uint8_t reserved1 : 1;
    union {
        struct
        {
            uint8_t Hour : 4;
            uint8_t Hour10 : 2;
            uint8_t reserved2 : 1;
            uint8_t hour_12_24 : 1; // 0 for 24 hour format
        } h24;
        struct
        {
            uint8_t Hour : 4;
            uint8_t Hour10 : 1;
            uint8_t AM_PM : 1; // 0 for AM, 1 for PM
            uint8_t reserved2 : 1;
            uint8_t hour_12_24 : 1; // 1 for 12 hour format
        } h12;
    };
    uint8_t Date : 4; // Day of month, 1 = first day
    uint8_t Date10 : 2;
    uint8_t reserved3 : 2;
    uint8_t Month : 4; // Month, 1 = January
    uint8_t Month10 : 1;
    uint8_t reserved4 : 3;
    uint8_t Day : 3; // Day of week, 1 = first day (any day)
    uint8_t reserved5 : 5;
    uint8_t Year : 4; // Year, 0 = year 2000
    uint8_t Year10 : 4;
    uint8_t reserved6 : 7;
    uint8_t WP : 1; // WP = Write Protect
};

void DS1302_clock_burst_read(uint8_t *p);
void DS1302_clock_burst_write(uint8_t *p);
