/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "state_idle.hh"
#include "alarm_time.hh"
#include "clock.hh"
#include "ds1302.hh"
#include "global_activity.hh"
#include "hardware.hh"
#include "light.hh"
#include "oled.hh"
#include "pushbuttons.hh"
#include "rotary_encoder.hh"
#include "segdisp.hh"
#include "state_alarm.hh"
#include "state_menu_top.hh"

StateIdle state_idle;

namespace {
void oled_brightness_update();
void on_brightness_knob_changed(float knob_position);
void rotenc_update();

void show_clock();
void clock_to_oled(const struct ds1302_struct &date_time);

const char *const days_of_week[] = {
    "Monday   ",
    "Tuesday  ",
    "Wednesday",
    "Thursday ",
    "Friday   ",
    "Saturday ",
    "Sunday   ",
};
}  // namespace

void StateIdle::enter()
{
  rotenc_reset();
  pushbuttons_reset();
  light_set_smoothing(false);
  if (light_on_) {
    light_on();
    light_set_intensity_level(light_intensity_index_);
  }
  else {
    light_off();
  }
  knob.set_callback(on_brightness_knob_changed);
  knob.reset();

  display.clear();
  display.draw_border();
  display.draw_progress_bar(light_get_visual_intensity());

  // Draw alarm time.
  display.set_text_position(4, 52);
  display.draw_time(alarm_time.hour, alarm_time.minute);

  // Draw alarm days of week.
  for (uint8_t dow = 0; dow < 7; dow++) {
    const uint8_t dow_mask = 1 << (dow + 1);
    if ((alarm_time.days_of_week & dow_mask) == 0)
      continue;
    display.set_text_position(50 + 8 * dow, 52);
    display.draw_character(days_of_week[dow][0]);
  }

  display.display();

  global_activity_mark();
}

void StateIdle::exit()
{
  knob.set_callback(nullptr);
  keep_current_light_intensity();
}

void StateIdle::tick()
{
  knob.update();
  show_clock();
  rotenc_update();
  oled_brightness_update();

  display.display();

  if (pushbuttons_get_clicks_and_reset_right()) {
    state_machine.go_to_state(&state_menu_top);
  }
  if (pushbuttons_get_clicks_and_reset_left()) {
    global_activity_mark();
  }

  if (alarm_time_is_now()) {
    state_machine.go_to_state(&state_alarm);
  }
}

void StateIdle::keep_current_light_intensity()
{
  light_intensity_index_ = light_intensity_index;
  light_on_ = light_is_on();
}

namespace {

void oled_brightness_update()
{
  // display.set_text_position(3, 18);

  const time_csec_t csec_since_activity = global_activity_time_since();
  if (csec_since_activity > 800) {
    // display.draw_text("OFF  ");
    display.ensure_power_off();
  }
  else if (csec_since_activity > 400 + 255) {
    // Do nothing, just wait until we hit the timeout above.
    // display.draw_text("WAIT ");
  }
  else if (csec_since_activity > 400) {
    // Darken over a period of 2 seconds.
    // display.draw_text("DIM  ");
    const uint16_t csec_since_darkening_started = (csec_since_activity - 400);
    const uint8_t oled_brightness = 255 - csec_since_darkening_started;
    display.set_contrast(oled_brightness);
  }
  else {
    // display.draw_text("ON   ");
    display.power_on();
    display.set_contrast(display.DEFAULT_CONTRAST);
  }
  // display.draw_uint(csec_since_activity);
}

void on_brightness_knob_changed(const float knob_position)
{
  const float intensity = knob_position * knob_position;
  segdisp_set_brightness(intensity * 255);
}

void rotenc_update()
{
  // Rotary encoder input
  int8_t rotenc_turns = rotenc_get_turns_and_reset();
  const bool rotenc_turned = rotenc_turns != 0;
  while (rotenc_turns > 0) {
    --rotenc_turns;
    light_brighter();
  }
  while (rotenc_turns < 0) {
    rotenc_turns++;
    light_darker();
  }
  if (rotenc_turned) {
    display.draw_progress_bar(light_get_visual_intensity());
  }
  if (rotenc_get_clicks_and_reset() > 0) {
    if (light_is_on()) {
      light_off();
    }
    else {
      light_on();
      global_activity_mark();
    }

    display.draw_progress_bar(light_get_visual_intensity());
  }
}

void clock_to_oled(const struct ds1302_struct &date_time)
{
  if (!display.is_power_on())
    return;

  // display.set_text_size(2);
  // display.set_text_position(13, 28);
  // display.draw_uint(date_time.h24.Hour10);
  // display.draw_uint(date_time.h24.Hour);
  // display.draw_character(':');
  // display.draw_uint(date_time.Minutes10);
  // display.draw_uint(date_time.Minutes);
  // display.draw_character(':');
  // display.draw_uint(date_time.Seconds10);
  // display.draw_uint(date_time.Seconds);

  display.set_text_size(2);
  display.set_text_position(4, 28);
  display.draw_uint(date_time.Date10);
  display.draw_uint(date_time.Date);
  display.draw_character('-');
  display.draw_uint(date_time.Month10);
  display.draw_uint(date_time.Month);
  display.draw_text("-20");
  display.draw_uint(date_time.Year10);
  display.draw_uint(date_time.Year);

  display.set_text_size(1);
  display.set_text_position(4, 17);
  display.draw_text(days_of_week[date_time.DayOfWeek - 1]);
}

void show_clock()
{
  if (!clock_update())
    return;

  segdisp_show_time(now);
  clock_to_oled(now);
}

}  // namespace
