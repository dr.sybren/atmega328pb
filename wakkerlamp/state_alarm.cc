/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "state_alarm.hh"
#include "alarm_time.hh"
#include "algorithm.hh"
#include "clock.hh"
#include "hardware.hh"
#include "light.hh"
#include "pushbuttons.hh"
#include "rotary_encoder.hh"
#include "segdisp.hh"
#include "state_idle.hh"
#include "timekeeping.hh"

#include <stdlib.h>

StateAlarm state_alarm;

void StateAlarm::enter()
{
  alarm_start_time_ = timekeeping_centisec();
  alarm_ended_due_to_timeout_ = false;

  display.set_text_size(2);
  display.power_on();
  display.set_contrast(0);
  display.clear();
  display.draw_text_centered(32, "ALARM");
  display.display();

  light_set_intensity_level(0);
  light_set_smoothing(true);
  light_on();

  rotenc_reset();
  pushbuttons_reset();
}

void StateAlarm::exit()
{
  alarm_time_turn_off();
  light_set_smoothing(false);
  segdisp_set_brightness(1);

  if (alarm_ended_due_to_timeout_) {
    light_off();
  }
  else {
    state_idle.keep_current_light_intensity();
  }
}

void StateAlarm::tick()
{
  show_clock();
  update_light_brightness();

  const float intensity = light_get_visual_intensity();
  const uint8_t intensity_8bit = intensity * 255;
  display.set_contrast(intensity_8bit);
  display.draw_progress_bar(intensity);

  check_alarm_duration();

  const uint8_t segdisp_brightness = intensity * intensity * 32;
  segdisp_set_brightness(max((uint8_t)1, segdisp_brightness));

  display.display();
  maybe_end_alarm();
}

void StateAlarm::update_light_brightness()
{
  static time_csec_t last_light_update = 0;
  constexpr time_csec_t brightening_duration_seconds = 600;
  constexpr time_csec_t csec_per_brightness_step = (100 * brightening_duration_seconds) /
                                                   LIGHT_INTENSITY_LEVELS;

  const time_csec_t now = timekeeping_centisec();
  if (now - last_light_update < csec_per_brightness_step) {
    display.set_text_size(1);
    display.set_text_position(3, 32);
    return;
  }

  last_light_update = now;
  light_brighter();
}

void StateAlarm::maybe_end_alarm()
{
  const int8_t turns = rotenc_get_turns_and_reset();
  if (turns < 0) {
    light_darker();
    end_alarm();
    return;
  }

  if (turns > 0) {
    light_brighter();
    end_alarm();
    return;
  }

  if (rotenc_get_clicks_and_reset() || pushbuttons_get_clicks_and_reset_left() ||
      pushbuttons_get_clicks_and_reset_right()) {
    end_alarm();
    return;
  }
}

void StateAlarm::check_alarm_duration()
{
  constexpr int32_t max_duration_mins = 60;
  constexpr time_csec_t max_duration_csec = max_duration_mins * 60 * 100;

  const time_csec_t alarm_duration = timekeeping_centisec() - alarm_start_time_;
  const time_csec_t alarm_time_left = max_duration_csec - alarm_duration;

  if (alarm_time_left <= 0) {
    alarm_ended_due_to_timeout_ = true;
    end_alarm();
    return;
  }

  const div_t time_left = div(alarm_time_left / 100, 60);
  display.set_text_position(48, 55);
  display.draw_time(time_left.quot, time_left.rem);
}

void StateAlarm::end_alarm()
{
  state_machine.go_to_state(&state_idle);
}

void StateAlarm::show_clock()
{
  if (!clock_update())
    return;

  segdisp_show_time(now);
}
