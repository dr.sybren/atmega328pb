# Wakkerlamp

This is an alarm clock that'll use LED strips to wake you up with light, instead of sound.

The [Hardware design](https://easyeda.com/dr.Sybren/wake-up-light) is available on EasyEDA.

## Pin-out

- LED-CTRL: PB2 / OC1B
- Knob: PC3 / ADC3
- Rotary Encoder: PD0 (clock), PD1 (data), adn PD2 (click)
- Pushbuttons: PC1 (right) and PC2 (left)

## Hardware components used

- Timer 0: Segment display
- Timer 1: Big LED PWM
- Timer 2: Knob input
- Timer 3: Timekeeping
- ADC: Knob input
- SPI0: Segment display
- TWI1: OLED display
- PCMSK1: Pushbuttons
- PCMSK2: Rotary Encoder

## License

The code in this project is licensed under the GPL v3 license, unless a different license is specified in the code file itself.

The non-code parts of this work are licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
