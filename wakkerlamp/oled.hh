/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include "ssd1306.hh"

class OLED : public SSD1306 {
 public:
  OLED(uint8_t i2c_addr = 0x3C);

  void draw_progress_bar(float progress, uint8_t y = 1);
  void draw_border();
  void draw_uint_twodigit(uint8_t number);
  void draw_time(uint8_t hours, uint8_t minutes);
};
