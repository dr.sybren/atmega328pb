/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include "state_idle.hh"
#include "state_menu_abstract.hh"

#include <stdint.h>

class StateMenuTime : public AbstractStateMenu {
 public:
  void enter() override;

 protected:
  bool editing_minutes_;
  int8_t hours_;
  int8_t minutes_;

  void draw() override;
  void on_rotate_left(uint8_t clicks) override;
  void on_rotate_right(uint8_t clicks) override;
  void on_click() override;
  void on_back() override;
};
extern StateMenuTime state_menu_time;
