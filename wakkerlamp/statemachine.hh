/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

class AbstractState {
 public:
  virtual void enter() = 0;
  virtual void exit() = 0;
  virtual void tick() = 0;
  virtual ~AbstractState(){};
};

class StateMachine {
  AbstractState *current_state_ = nullptr;
  AbstractState *next_state_ = nullptr;

 public:
  void tick();

  void go_to_state(AbstractState *new_state);

 private:
  void go_to_next_state();
};

extern StateMachine state_machine;
