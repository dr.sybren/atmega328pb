/* Wakkerlamp: Rotary Encoder
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "rotary_encoder.hh"
#include "global_activity.hh"
#include "light.hh"
#include "segdisp.hh"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/atomic.h>

#include <stdint.h>

constexpr uint8_t ROTENC_CLOCK_PIN_MASK = (1 << PORTD0);
constexpr uint8_t ROTENC_DATA_PIN_MASK = (1 << PORTD1);
constexpr uint8_t ROTENC_CLICK_PIN_MASK = (1 << PORTD2);
constexpr uint8_t ROTENC_ALL_PIN_MASK = ROTENC_CLOCK_PIN_MASK | ROTENC_DATA_PIN_MASK |
                                        ROTENC_CLICK_PIN_MASK;

volatile int8_t rotenc_turns;
volatile uint8_t rotenc_clicks;

void rotary_encoder_setup()
{
  rotenc_turns = 0;

  // Set pins to input with pull-up resistors.
  DDRD &= ~ROTENC_ALL_PIN_MASK;
  PORTD |= ROTENC_ALL_PIN_MASK;

  // Wait for pull-up resistors to do their work.
  while ((PORTD & ROTENC_ALL_PIN_MASK) != ROTENC_ALL_PIN_MASK)
    ;

  for (uint16_t delay = 0; delay < 3000; ++delay)
    __asm__ __volatile__("nop");

  // Configure pin change interrupts.
  PCICR |= 1 << PCIE2;
  PCMSK2 |= 0               //
            | 1 << PCINT16  // Clock: PD0
            | 1 << PCINT18  // Click: PD2
      ;

  // Clear any interrupt flag that was set due to the pins being high.
  PCIFR = 0;
}

void rotenc_reset()
{
  rotenc_turns = 0;
  rotenc_clicks = 0;
}

int8_t rotenc_get_turns_and_reset()
{
  int8_t turns;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    turns = rotenc_turns;
    rotenc_turns = 0;
  }
  return turns;
}

uint8_t rotenc_get_clicks_and_reset()
{
  uint8_t clicks;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    clicks = rotenc_clicks;
    rotenc_clicks = 0;
  }
  return clicks;
}

void on_rotenc_click()
{
  static time_csec_t last_click = 0;
  const time_csec_t now = timekeeping_centisec();
  if (now - last_click < 50)
    return;
  last_click = now;

  if (rotenc_clicks < 125)
    ++rotenc_clicks;
}

void on_rotenc_rotate(bool rotated_right)
{
  global_activity_mark();

  if (rotated_right) {
    if (rotenc_turns < 125)
      ++rotenc_turns;
  }
  else {
    if (rotenc_turns > -125)
      --rotenc_turns;
  }
}

ISR(PCINT2_vect)
{
  // Check click
  const uint8_t pinD = PIND;
  if ((pinD & ROTENC_CLICK_PIN_MASK) == 0) {
    on_rotenc_click();
    return;
  }

  // Check clock & data
  if ((pinD & ROTENC_CLOCK_PIN_MASK) == 0) {
    on_rotenc_rotate((pinD & ROTENC_DATA_PIN_MASK) == 0);
  }
}
