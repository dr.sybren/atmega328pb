/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "clock.hh"
#include "timekeeping.hh"

#include <string.h>

ds1302_struct now;
bool clock_was_updated;

bool clock_update()
{
  static time_csec_t last_clock_update = 0;

  const time_csec_t centisec = timekeeping_centisec();
  if (centisec - last_clock_update < 100) {
    return false;
  }
  last_clock_update = centisec;

  ds1302_struct back_then = now;
  DS1302_clock_burst_read(&now);
  return memcmp(&back_then, &now, sizeof(now)) != 0;
}
