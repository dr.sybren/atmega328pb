/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <avr/pgmspace.h>
#include <stdint.h>

// LED-CTRL: PB2 / OC1B

// Mapping from a single "step" to PWM value.
static const uint16_t light_map_10bit[] PROGMEM = {
    0,  1,  2,  3,   4,   5,   7,   9,   11,  14,  18,  22,  28,  35,  43,
    54, 67, 83, 102, 126, 156, 192, 237, 293, 361, 444, 548, 674, 831, 1023};
constexpr uint8_t LIGHT_INTENSITY_LEVELS = sizeof(light_map_10bit) / sizeof(light_map_10bit[0]);
extern volatile uint8_t light_intensity_index;

void light_setup();

void light_set_pwm(uint16_t pwm_value);
float light_get_visual_intensity();

// 0 = dimmest but on, LIGHT_INTENSITY_LEVELS-1 = brightest
void light_set_intensity_level(uint8_t level);
void light_brighter();
void light_darker();

bool light_is_on();
void light_on();
void light_off();
void light_blink();
void light_toggle();

void light_set_smoothing(bool is_smoothing);

void light_debug_draw();
