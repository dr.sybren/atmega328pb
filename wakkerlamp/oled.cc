/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "oled.hh"

#include <stdlib.h>

OLED::OLED(const uint8_t i2c_addr) : SSD1306(i2c_addr)
{
}

void OLED::draw_progress_bar(const float progress, const uint8_t top_y)
{
  const uint8_t text_y = top_y + 2;
  const uint16_t bar_width = progress * (SCREEN_WIDTH - 2);
  draw_progress_bar_horiz(1, top_y, SCREEN_WIDTH - 2, top_y + 13, bar_width);

  set_text_size(1);
  if (progress < 0.5f) {
    set_text_inverted(false);
    set_text_position(SCREEN_WIDTH / 2 + 10, text_y);
  }
  else {
    set_text_inverted(true);
    set_text_position(10, text_y);
  }

  if (progress < 0.10f) {
    // Draw in center, one character to the right.
    set_text_position(46 + 6, text_y);
  }
  else if (progress < 0.25f || 0.75f < progress) {
    // Draw in center.
    set_text_position(46, text_y);
  }
  else if (progress < 0.5f) {
    // Draw right.
    set_text_position(82, text_y);
  }
  else {
    // Draw left.
    set_text_position(10, text_y);
  }
  draw_float(progress * 100.f);
  draw_text("%");
  set_text_inverted(false);
}

void OLED::draw_border()
{
  draw_rect(0, 0, SCREEN_WIDTH - 1, SCREEN_HEIGHT - 1);
  draw_line(0, 15, SCREEN_WIDTH - 1, 15);
}

void OLED::draw_uint_twodigit(uint8_t number)
{
  const div_t divided = div(number, 10);
  draw_character(divided.quot + '0');
  draw_character(divided.rem + '0');
}

void OLED::draw_time(uint8_t hours, uint8_t minutes)
{
  draw_uint_twodigit(hours);
  draw_character(':');
  draw_uint_twodigit(minutes);
}
