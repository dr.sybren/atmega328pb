/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "statemachine.hh"

StateMachine state_machine;

void StateMachine::tick()
{
  if (current_state_)
    current_state_->tick();
  if (next_state_)
    go_to_next_state();
}

void StateMachine::go_to_state(AbstractState *new_state)
{
  next_state_ = new_state;
}

void StateMachine::go_to_next_state()
{
  if (current_state_) {
    current_state_->exit();
  }
  current_state_ = next_state_;
  next_state_ = nullptr;
  current_state_->enter();
}
