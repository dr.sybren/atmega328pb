/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ssd1306.hh"
#include "ssd1306_font.hh"

extern "C" {
#include "i2cmaster.h"

#include <avr/io.h>
#include <avr/pgmspace.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
}

SSD1306::SSD1306(uint8_t i2c_addr) : i2c_addr_(i2c_addr)
{
}

bool SSD1306::init()
{
  if (!buffer_) {
    buffer_ = (uint8_t *)malloc(BUFFER_SIZE);
    clear();
  }

  // Enable the internal pull-upresistors on the i2c pins.
  PORTE |= (1 << PORTE0) | (1 << PORTE1);

  i2c_init();
  const bool device_ok = i2c_start(i2c_addr_, I2C_WRITE) == 0;
  i2c_stop();

  if (!device_ok) {
    return false;
  }

  configure_hardware();
  return true;
}

void SSD1306::draw_pixel(const int16_t x, const int16_t y, const PixelColor color)
{
  if (x < 0 || x >= SCREEN_WIDTH || y < 0 || y >= SCREEN_HEIGHT)
    return;

  uint8_t *pixel_group = &buffer_[x + (y / 8) * SCREEN_WIDTH];
  uint8_t pixel_bit = 1 << (y & 7);

  switch (color) {
    case PixelColor::WHITE:
      *pixel_group |= pixel_bit;
      break;
    case PixelColor::BLACK:
      *pixel_group &= ~pixel_bit;
      break;
    case PixelColor::INVERT:
      *pixel_group ^= pixel_bit;
      break;
  }
}

void SSD1306::draw_line(
    int16_t x0, int16_t y0, const int16_t x1, const int16_t y1, const PixelColor color)
{
  const int16_t dx = abs(x1 - x0);
  const int16_t sx = x0 < x1 ? 1 : -1;
  const int16_t dy = -abs(y1 - y0);
  const int16_t sy = y0 < y1 ? 1 : -1;

  int16_t err = dx + dy; /* error value e_xy */
  while (true) {
    draw_pixel(x0, y0, color);
    if (x0 == x1 && y0 == y1)
      break;
    const int16_t e2 = 2 * err;
    if (e2 >= dy) { /* e_xy+e_x > 0 */
      err += dy;
      x0 += sx;
    }
    if (e2 <= dx) { /* e_xy+e_y < 0 */
      err += dx;
      y0 += sy;
    }
  }
}
void SSD1306::draw_rect(
    const int16_t x0, const int16_t y0, const int16_t x1, const int16_t y1, const PixelColor color)
{
  draw_line(x0, y0, x1, y0, color);
  draw_line(x1, y0, x1, y1, color);
  draw_line(x1, y1, x0, y1, color);
  draw_line(x0, y1, x0, y0, color);
}
void SSD1306::draw_rect_filled(
    const int16_t x0, int16_t y0, const int16_t x1, const int16_t y1, const PixelColor color)
{
  for (; y0 <= y1; ++y0) {
    draw_line(x0, y0, x1, y0, color);
  }
}

void SSD1306::draw_progress_bar_horiz(
    int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint8_t bar_width)
{
  if (bar_width < x1) {
    draw_rect_filled(x0 + bar_width, y0, x1, y1, PixelColor::BLACK);
  }
  if (bar_width > 0) {
    draw_rect_filled(x0, y0, x0 + bar_width, y1, PixelColor::WHITE);
  }
}

void SSD1306::set_text_size(uint8_t size)
{
  text_size_ = size;
}
void SSD1306::set_text_position(int16_t x, int16_t y)
{
  text_x_ = x;
  text_y_ = y;
}
void SSD1306::set_text_inverted(bool is_inverted)
{
  is_text_inverted_ = is_inverted;
}
void SSD1306::draw_text(const char *text)
{
  for (; *text; ++text) {
    draw_character(*text);
  }
}

void SSD1306::draw_float(float value)
{
  if (value < 0) {
    draw_character('-');
    value = fabs(value);
  }

  char buf[15];
  uint16_t decimals = fmod(value, 1) * 10;
  sprintf(buf, "%d.%01d", (int)value, decimals);
  draw_text(buf);
}

void SSD1306::draw_uint(uint16_t value)
{
  char buf[8];
  itoa(value, buf, 10);
  draw_text(buf);
}
void SSD1306::draw_int(int8_t value)
{
  char buf[5];
  itoa(value, buf, 10);
  draw_text(buf);
}

void SSD1306::draw_text_centered(const int16_t y, const char *const text)
{
  text_x_ = (SCREEN_WIDTH - text_width(text)) / 2;
  text_y_ = y;
  draw_text(text);
}

uint16_t SSD1306::text_width(const char *text)
{
  return 6 * text_size_ * strlen(text);
}

void SSD1306::draw_character(const char c)
{
  for (int8_t column = 0; column < 5; column++) {  // Char bitmap = 5 columns
    uint8_t line = pgm_read_byte(&font[c * 5 + column]);
    for (int8_t row = 0; row < 8; row++, line >>= 1) {
      const PixelColor pixel_color = (line & 1) == is_text_inverted_ ? PixelColor::BLACK :
                                                                       PixelColor::WHITE;
      const uint16_t pixel_x = text_x_ + text_size_ * column;
      const uint16_t pixel_y = text_y_ + text_size_ * row;
      if (text_size_ == 1) {
        draw_pixel(pixel_x, pixel_y, pixel_color);
      }
      else {
        draw_rect_filled(
            pixel_x, pixel_y, pixel_x + text_size_, pixel_y + text_size_, pixel_color);
      }
    }
  }

  const uint8_t x_step = 6 * text_size_;
  text_x_ += x_step;
}

void SSD1306::clear()
{
  memset(buffer_, 0, BUFFER_SIZE);
}
void SSD1306::clear_lower()
{
  constexpr size_t offset = SCREEN_WIDTH * 16 / 8;
  memset(buffer_ + offset, 0, BUFFER_SIZE - offset);
}

void SSD1306::display()
{
  if (!power_) {
    return;
  }

  set_page_address(0, 0x7F);
  set_column_address(0, SCREEN_WIDTH - 1);

  // Send the buffer in 32-byte chunks.
  constexpr uint8_t chunk_size = 32;
  constexpr uint16_t num_bytes_full_chunks = BUFFER_SIZE & ~(chunk_size - 1);
  constexpr uint8_t num_bytes_remaining = num_bytes_full_chunks & (chunk_size - 1);

  uint8_t *remaining_buffer = buffer_;
  for (uint16_t num_bytes_written = 0; num_bytes_written < num_bytes_full_chunks;
       num_bytes_written += chunk_size, remaining_buffer += chunk_size) {
    send_data_buffer(remaining_buffer, chunk_size);
  }
  if constexpr (num_bytes_remaining > 0) {
    send_data_buffer(remaining_buffer, num_bytes_remaining);
  }
}

void SSD1306::send_data_buffer(const uint8_t *buffer, uint8_t buffer_size)
{
  i2c_start_wait(i2c_addr_, I2C_WRITE);
  i2c_write(0x40);  // Indicates this is data, not a command.
  while (buffer_size--) {
    i2c_write(*buffer++);
  }
  i2c_stop();
}

void SSD1306::configure_hardware()
{
  // The hardware config routine was copied from the Adafruit SSD1306 driver.
  power(false);
  set_display_clockdiv(0x80);
  set_multiplex_ratio(SCREEN_HEIGHT - 1);
  set_display_offset(0);
  set_display_start_line(0);
  set_charge_pump(true);
  set_memory_mode(0);  // Horizontal Addressing Mode
  set_segment_remap(true);
  set_com_output_scan_direction(true);
  set_com_pins(0b01);
  set_contrast(DEFAULT_CONTRAST);
  set_precharge_period(0xF1);
  set_vcomh_deselect_level(0);
  entire_display_on(false);
  inverse_display(false);
  scroll_active(false);
  power(true);
}

// 1. Fundamental command table.
void SSD1306::set_contrast(uint8_t level)
{
  send_command(0x81, level);
}
void SSD1306::entire_display_on(bool ignore_ram_content)
{
  send_command(0xA4 | ignore_ram_content);
}
void SSD1306::inverse_display(bool set_inverted)
{
  send_command(0xA6 | set_inverted);
}
void SSD1306::power(bool set_display_on)
{
  send_command(0xAE | set_display_on);
  power_ = set_display_on;
}
void SSD1306::ensure_power_on()
{
  if (!power_)
    power_on();
}
void SSD1306::ensure_power_off()
{
  if (power_)
    power_off();
}

void SSD1306::power_on()
{
  send_command(0xAF);
  power_ = true;
}
void SSD1306::power_off()
{
  send_command(0xAE);
  power_ = false;
}
bool SSD1306::is_power_on()
{
  return power_;
}

// 2. Scrolling Command Table
void SSD1306::scroll_active(bool scrolling_active)
{
  send_command(0x2E | scrolling_active);
}

// 3. Addressing Setting Command Table
void SSD1306::set_column_start_address(uint8_t column_start_address)
{
  send_command(0x00 | (column_start_address & 0x0F));
  send_command(0x10 | (column_start_address >> 4));
}
void SSD1306::set_memory_mode(uint8_t mode)
{
  send_command(0x20, mode & 0b11);
}
void SSD1306::set_column_address(uint8_t start_address, uint8_t end_address)
{
  send_command(0x21, start_address, end_address);
}
void SSD1306::set_page_address(uint8_t start_address, uint8_t end_address)
{
  send_command(0x22, start_address, end_address);
}

// 4. Hardware Configuration (Panel resolution & layout related) Command Table
void SSD1306::set_display_start_line(uint8_t start_line)
{
  send_command(0x40 | (start_line & 0x3F));
}
void SSD1306::set_segment_remap(bool map_column_127)
{
  send_command(0xA0 | map_column_127);
}
void SSD1306::set_multiplex_ratio(uint8_t ratio)
{
  send_command(0xA8, ratio & 0x3F);
}
void SSD1306::set_com_output_scan_direction(bool remapped_mode)
{
  send_command(0xC0 | (remapped_mode << 3));
}
void SSD1306::set_display_offset(uint8_t offset)
{
  send_command(0xD3, offset & 0x3F);
}
void SSD1306::set_com_pins(uint8_t pin_config)
{
  send_command(0xDA, 0x02 | (pin_config << 4));
}

// 5. Timing & Driving Scheme Setting Command Table
void SSD1306::set_display_clockdiv(uint8_t clockdiv)
{
  send_command(0xD5, clockdiv);
}
void SSD1306::set_precharge_period(uint8_t phase_periods)
{
  send_command(0xD9, phase_periods);
}
void SSD1306::set_vcomh_deselect_level(uint8_t level)
{
  send_command(0xDB, (level & 0b111) << 4);
}

// Charge Pump Command Table
void SSD1306::set_charge_pump(bool enable_charge_pump)
{
  send_command(0x8D, 0x10 | (enable_charge_pump << 2));
}

// Low-level functions
void SSD1306::send_command(uint8_t byte)
{
  i2c_start(i2c_addr_, I2C_WRITE);
  i2c_write(0);  // Indicate "control" rather than "data".
  i2c_write(byte);
  i2c_stop();
}
void SSD1306::send_command(uint8_t byte1, uint8_t byte2)
{
  i2c_start(i2c_addr_, I2C_WRITE);
  i2c_write(0);  // Indicate "control" rather than "data".
  i2c_write(byte1);
  i2c_write(byte2);
  i2c_stop();
}
void SSD1306::send_command(uint8_t byte1, uint8_t byte2, uint8_t byte3)
{
  i2c_start(i2c_addr_, I2C_WRITE);
  i2c_write(0);  // Indicate "control" rather than "data".
  i2c_write(byte1);
  i2c_write(byte2);
  i2c_write(byte3);
  i2c_stop();
}
