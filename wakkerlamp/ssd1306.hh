/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <stdint.h>

enum class PixelColor : uint8_t {
  BLACK = 0,
  WHITE = 1,
  INVERT = 2,
};

// Just hard-code for my display.
const int SCREEN_HEIGHT = 64;
const int SCREEN_WIDTH = 128;
constexpr int BUFFER_SIZE = SCREEN_WIDTH * ((SCREEN_HEIGHT + 7) / 8);

class SSD1306 {
 private:
  uint8_t i2c_addr_;
  uint8_t *buffer_ = nullptr;
  bool power_ = true;

  uint8_t text_size_ = 1;
  int16_t text_x_ = 0;
  int16_t text_y_ = 0;
  bool is_text_inverted_ = false;

 public:
  static const uint8_t DEFAULT_CONTRAST = 0xCF;

  SSD1306(uint8_t i2c_addr = 0x3C);

  /* Return "device ok". */
  bool init();

  void clear();
  void clear_lower();
  void display();

  // Drawing functions.
  void draw_pixel(int16_t x, int16_t y, PixelColor color = PixelColor::WHITE);
  void draw_line(
      int16_t x0, int16_t y0, int16_t x1, int16_t y1, PixelColor color = PixelColor::WHITE);
  void draw_rect(
      int16_t x0, int16_t y0, int16_t x1, int16_t y1, PixelColor color = PixelColor::WHITE);
  void draw_rect_filled(
      int16_t x0, int16_t y0, int16_t x1, int16_t y1, PixelColor color = PixelColor::WHITE);
  void draw_progress_bar_horiz(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint8_t bar_width);

  void set_text_size(uint8_t size);
  void set_text_position(int16_t x, int16_t y);
  void set_text_inverted(bool is_inverted);

  void draw_text(const char *text);
  void draw_float(float value);
  void draw_uint(uint16_t value);
  void draw_int(int8_t value);

  void draw_text_centered(int16_t y, const char *text);
  void draw_character(char character);

  /* Return text width in pixels, given the current text size. */
  uint16_t text_width(const char *text);

  // 1. Fundamental command table.
  void set_contrast(uint8_t level);
  void entire_display_on(bool ignore_ram_content);
  void inverse_display(bool set_inverted);
  void power(bool set_display_on);
  void power_on();
  void power_off();
  void ensure_power_on();
  void ensure_power_off();
  bool is_power_on();

  // 2. Scrolling Command Table
  void scroll_active(bool scrolling_active);

 private:
  void send_command(uint8_t byte);
  void send_command(uint8_t byte1, uint8_t byte2);
  void send_command(uint8_t byte1, uint8_t byte2, uint8_t byte3);
  void send_data_buffer(const uint8_t *buffer, uint8_t buffer_size);

  void configure_hardware();

  // 3. Addressing Setting Command Table
  void set_column_start_address(uint8_t column_start_address);
  void set_memory_mode(uint8_t mode);
  void set_column_address(uint8_t start_address, uint8_t end_address);
  void set_page_address(uint8_t start_address, uint8_t end_address);

  // 4. Hardware Configuration (Panel resolution & layout related) Command Table
  void set_display_start_line(uint8_t start_line);
  void set_segment_remap(bool map_column_127);
  void set_multiplex_ratio(uint8_t ratio);
  void set_com_output_scan_direction(bool remapped_mode);
  void set_display_offset(uint8_t offset);
  void set_com_pins(uint8_t pin_config);

  // 5. Timing & Driving Scheme Setting Command Table
  void set_display_clockdiv(uint8_t clockdiv);
  void set_precharge_period(uint8_t phase_periods);
  void set_vcomh_deselect_level(uint8_t level);

  // Charge Pump Command Table
  void set_charge_pump(bool enable_charge_pump);
};
