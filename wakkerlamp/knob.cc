/* ATmega328pb Wakkerlamp
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "knob.hh"
#include "algorithm.hh"

#include <math.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>

volatile static bool knob_may_update = true;

void Knob::setup()
{
  // Don't disable the ADC.
  PRR0 &= (1 << PRADC);

  // Disable digital input on ADC3 / PC3 and set as input.
  DIDR0 |= 1 << ADC3D;
  DDRC &= ~(1 << PC3);

  ADMUX = 0                 //
          | 0b01 << REFS0   // AVCC with external capacitor at AREF pin
          | 0b0011 << MUX0  // Read from ADC3 / PC3
      ;
  ADCSRA = 0                 //
           | 1 << ADEN       // Enable ADC
           | 1 << ADATE      // Auto-trigger enable
           | 0b111 << ADPS0  // scale ADC clock so it's 50-200 kHz
      ;
  ADCSRB = 0                 //
           | 0b101 << ADTS0  // Auto-trigger on Timer 0 channel A (100 Hz)
      ;

  // Use timer 2 for pacing the updates.
  PRR0 &= (1 << PRTIM2);

  // WGM = 0b010 for CTC mode.
  TCCR2A = 0                //
           | 0b10 << WGM20  //
      ;
  TCCR2B = 0                //
           | 0b0 << WGM22   //
           | 0b101 << CS20  // 1/1024 clockdiv
      ;
  OCR2A = 255;  // should give appox. 30 Hz
  TIMSK2 = 1 << OCIE2A;
  TCNT2 = 0;
}

ISR(TIMER2_COMPA_vect)
{
  knob_may_update = true;
}

bool Knob::update()
{
  if (!knob_may_update) {
    return false;
  }
  knob_may_update = false;

  const float knob_position = get_smooth_position();
  const float movement = fabs(last_position_ - knob_position);

  last_position_ = knob_position;
  if (movement < 0.0001f) {
    return false;
  }

  if (on_value_changed_ != nullptr) {
    on_value_changed_(knob_position);
  }
  return true;
}

void Knob::reset()
{
  last_position_ = -100.0f;
  smoothed_position_ = 0.0f;
  update();
}

float Knob::position() const
{
  return last_position_;
}

float Knob::get_abs_position()
{
  const uint16_t ADC_MARGIN = 45;
  const uint16_t adc = ADC;
  if (adc <= ADC_MARGIN) {
    return 0.0f;
  }
  const float knob_position = (adc - ADC_MARGIN) / (1023.0f - ADC_MARGIN);
  return max(0.0f, min(knob_position, 1.0f));
}

float Knob::get_smooth_position()
{
  const float abs_position = get_abs_position();
  const float knob_diff = abs_position - smoothed_position_;

  if (knob_diff < 1e-3) {
    smoothed_position_ = abs_position;
    return abs_position;
  }

  smoothed_position_ += 0.25f * knob_diff;
  return smoothed_position_;
}

void Knob::set_callback(callback on_value_changed)
{
  on_value_changed_ = on_value_changed;
}
