/* ATmega328pb TLC6C598QPWRQ1 8-channel LED driver for segment display.
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "segdisp.hh"

#include <stdlib.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

// Constants for pin assignments & masks.
namespace {
const uint8_t SEGDISP_DIGIT_DISABLE = PD5;
const uint8_t SEGDISP_DIGIT_REGISTER = PD6;
const uint8_t SEGDISP_DIGIT1 = PB7;
const uint8_t SEGDISP_DIGIT2 = PB1;
const uint8_t SEGDISP_DIGIT3 = PB0;
const uint8_t SEGDISP_DIGIT4 = PD7;
const uint8_t SEGDISP_MOSI = PB3;
const uint8_t SEGDISP_SCK = PB5;

constexpr uint8_t SEGDISP_DIGIT_DISABLE_MASK = (1 << SEGDISP_DIGIT_DISABLE);
constexpr uint8_t SEGDISP_DIGIT_REGISTER_MASK = (1 << SEGDISP_DIGIT_REGISTER);
constexpr uint8_t SEGDISP_DIGIT1_MASK = (1 << SEGDISP_DIGIT1);
constexpr uint8_t SEGDISP_DIGIT2_MASK = (1 << SEGDISP_DIGIT2);
constexpr uint8_t SEGDISP_DIGIT3_MASK = (1 << SEGDISP_DIGIT3);
constexpr uint8_t SEGDISP_DIGIT4_MASK = (1 << SEGDISP_DIGIT4);
constexpr uint8_t SEGDISP_MOSI_MASK = (1 << SEGDISP_MOSI);
constexpr uint8_t SEGDISP_SCK_MASK = (1 << SEGDISP_SCK);

constexpr uint8_t SEGDISP_PORTB_MASK = SEGDISP_DIGIT1_MASK | SEGDISP_DIGIT2_MASK |
                                       SEGDISP_DIGIT3_MASK | SEGDISP_MOSI_MASK | SEGDISP_SCK_MASK;
constexpr uint8_t SEGDISP_PORTD_MASK = SEGDISP_DIGIT_DISABLE_MASK | SEGDISP_DIGIT_REGISTER_MASK |
                                       SEGDISP_DIGIT4_MASK;

const uint8_t SEGDISP_BIT_DECIMAL_POINT_MASK = (1 << 2);

const uint8_t SEGDISP_DIGIT_SEGMENTS[] = {
    0xeb, 0x28, 0xb3, 0xb9, 0x78, 0xd9, 0xdb, 0xa8, 0xfb, 0xf9};

}  // namespace

volatile bool segdisp_spi_bus_free;

uint8_t segdisp_digit_values[4];
uint8_t segdisp_decpoint_position;           // 0..4 ; 4 = no decimal point
volatile uint8_t segdisp_current_digit_idx;  // 0..3

// Private functions
namespace {
void disable_digit_pnps()
{
  // Disable all digits by pulling the pins to the PNP transistors high.
  PORTB |= SEGDISP_DIGIT1_MASK | SEGDISP_DIGIT2_MASK | SEGDISP_DIGIT3_MASK;
  PORTD |= SEGDISP_DIGIT4_MASK;
}

void enable_pnp_for(const uint8_t digit_idx)
{
  // Pull the pin low to enable the digit.
  switch (digit_idx) {
    case 0:
      PORTB &= ~SEGDISP_DIGIT1_MASK;
      break;
    case 1:
      PORTB &= ~SEGDISP_DIGIT2_MASK;
      break;
    case 2:
      PORTB &= ~SEGDISP_DIGIT3_MASK;
      break;
    case 3:
      PORTD &= ~SEGDISP_DIGIT4_MASK;
      break;
  }
}

void segdisp_flush()
{
  disable_digit_pnps();

  // Pulse "Digit Register" to push the shift register data to its outputs.
  PORTD |= SEGDISP_DIGIT_REGISTER_MASK;
  PORTD &= ~SEGDISP_DIGIT_REGISTER_MASK;

  enable_pnp_for(segdisp_current_digit_idx);
  segdisp_spi_bus_free = true;
}

void send_current_digit_value()
{
  segdisp_spi_bus_free = false;

  const uint8_t digit_value = segdisp_digit_values[segdisp_current_digit_idx];

  uint8_t segments = SEGDISP_DIGIT_SEGMENTS[digit_value];
  if (segdisp_current_digit_idx == segdisp_decpoint_position) {
    segments |= SEGDISP_BIT_DECIMAL_POINT_MASK;
  }

  SPDR0 = segments;
}

}  // namespace

void segdisp_show_number(uint16_t number)
{
  for (uint8_t digit_idx = 0; digit_idx < 4; ++digit_idx) {
    segdisp_digit_values[digit_idx] = number % 10;
    number /= 10;
  }
}
void segdisp_show_digits(uint8_t digit3, uint8_t digit2, uint8_t digit1, uint8_t digit0)
{
  segdisp_digit_values[0] = digit0;
  segdisp_digit_values[1] = digit1;
  segdisp_digit_values[2] = digit2;
  segdisp_digit_values[3] = digit3;
}

void segdisp_set_decpoint(const uint8_t digit_idx)
{
  segdisp_decpoint_position = digit_idx;
}

void segdisp_enable_digit(const uint8_t digit_idx)
{
  disable_digit_pnps();
  enable_pnp_for(digit_idx);
}

// Brightness functions.
namespace {
void bright_off()
{
  PORTD |= SEGDISP_DIGIT_DISABLE_MASK;
  PRR0 |= 1 << PRTIM0;
}
void bright_low(const uint8_t brightness)
{
  PRR0 &= ~(1 << PRTIM0);  // Enable Timer0
  TIMSK0 |= 1 << OCIE0B;   // Enable output compare interrupt
  OCR0B = brightness - 1;
}
void bright_high(const uint8_t brightness)
{
  // Reconnect the Digit Disable pin to the PWM hardware.
  PORTD &= ~SEGDISP_DIGIT_DISABLE_MASK;
  TCCR0A |= 0b11 << COM0B0;

  PRR0 &= ~(1 << PRTIM0);    // Enable Timer0
  TIMSK0 &= ~(1 << OCIE0B);  // Disable output compare interrupt
  OCR0B = brightness - 1;
}
bool is_bright()
{
  return (TIMSK0 & (1 << OCIE0B)) == 0;
}
}  // namespace

void segdisp_set_brightness(const uint8_t brightness)
{
  if (brightness == 0) {
    bright_off();
    return;
  }

  const uint8_t threshold = 8;
  if (brightness <= threshold) {
    bright_low(brightness);
  }
  else {
    bright_high(brightness - (threshold >> 1));
  }
}

void segdisp_show_time(const struct ds1302_struct &date_time)
{
  segdisp_show_digits(
      date_time.h24.Hour10, date_time.h24.Hour, date_time.Minutes10, date_time.Minutes);
}

void segdisp_show_time(uint8_t hours, uint8_t minutes)
{
  const div_t hours_div = div(hours, 10);
  const div_t minutes_div = div(minutes, 10);
  segdisp_show_digits(hours_div.quot, hours_div.rem, minutes_div.quot, minutes_div.rem);
}

void segdisp_setup()
{
  // Set ports to output.
  DDRB |= SEGDISP_PORTB_MASK;
  DDRD |= SEGDISP_PORTD_MASK;

  // Disable all digits.
  segdisp_enable_digit(0);
  PORTD |= SEGDISP_DIGIT_DISABLE_MASK;

  // Disable power reduction for SPI0 and Timer0.
  PRR0 &= ~(1 << PRSPI0 | 1 << PRTIM0);

  // Enable SPI, Master, set clock rate fck/16
  SPCR0 = 0                //
          | 1 << SPIE      // Interrupt enable
          | 1 << SPE       // SPI0 enable
          | 0 << DORD      // LSB first
          | 1 << MSTR      // Master mode
          | 0 << CPOL      // Clock idles LOW
          | 0b001 << SPR0  // Clock 1/16
      ;

  // Set Timer0 to PWM the DIGIT_DISABLE pin.
  TCCR0A = 0                 //
           | 0b11 << COM0B0  // Inverting mode
           | 0b11 << WGM00   // Fast PWM mode
      ;
  TCCR0B = 0                //
           | 0b0 << WGM02   //
           | 0b010 << CS00  // clockdiv
      ;
  TIMSK0 = 0              //
           | 1 << OCIE0B  // Output compare interrupt
           | 1 << TOIE0   // Overflow interrupt enable
      ;
  OCR0B = 2;

  segdisp_spi_bus_free = true;
  segdisp_current_digit_idx = 0;
  segdisp_decpoint_position = 0;
}

ISR(SPI0_STC_vect)
{
  segdisp_flush();
}

ISR(TIMER0_OVF_vect)
{
  if (!segdisp_spi_bus_free)
    return;

  // Move to each next digit at a slower pace than the software PWM.
  static uint8_t interrupt_counter = 0;
  ++interrupt_counter;
  if (OCR0B > 200) {
    if (interrupt_counter)
      return;
  }
  else {
    if (interrupt_counter & 0b1111)
      return;
  }
  interrupt_counter = 0;

  segdisp_current_digit_idx = (segdisp_current_digit_idx + 1) & 0b11;
  send_current_digit_value();
}

ISR(TIMER0_COMPB_vect)
{
  static uint8_t interrupt_counter = 0;
  ++interrupt_counter;

  if ((interrupt_counter & 0b11) == 0) {
    // Connect the PWM pin.
    TCCR0A |= 0b11 << COM0B0;
    PORTD &= ~SEGDISP_DIGIT_DISABLE_MASK;
  }
  else {
    // Disconnect the PWM pin.
    TCCR0A &= ~(0b11 << COM0B0);
    PORTD |= SEGDISP_DIGIT_DISABLE_MASK;
  }
}
