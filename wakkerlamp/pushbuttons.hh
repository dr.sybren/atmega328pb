/* Wakkerlamp: pushbuttons
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

// Pinout:
//   Button RIGHT: PC1 PCINT9
//   Button LEFT : PC2 PCINT10

#include <stdint.h>

void pushbuttons_setup();
void pushbuttons_reset();
uint8_t pushbuttons_get_clicks_and_reset_left();
uint8_t pushbuttons_get_clicks_and_reset_right();
