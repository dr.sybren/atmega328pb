/* ATmega328pb test with TLC6C598QPWRQ1 8-channel LED driver.
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "segdisp.hh"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

#include <math.h>

// void loop()
// {
//   segdisp_show_number(74);
//   for (uint16_t delay = 0; delay < 1000; ++delay) {
//     __asm__ __volatile__("nop");
//   }
// }

int main()
{
  // For simplification: set all pins to low output.
  DDRB = DDRD = 255;
  PORTB = PORTD = 0;

  segdisp_setup();
  segdisp_set_decpoint(0);

  sei();

  // Set ADC to continuous polling
  // Don't disable the ADC.
  PRR0 &= (1 << PRADC);

  // Disable digital input on ADC3 / PC3.
  DIDR0 |= 1 << ADC3D;

  ADMUX = 0                 //
          | 0b01 << REFS0   // AVCC with external capacitor at AREF pin
          | 0b0011 << MUX0  // Read from ADC3 / PC3
      ;
  ADCSRA = 0                 //
           | 1 << ADEN       // Enable ADC
           | 1 << ADATE      // Auto-trigger enable
           | 0b111 << ADPS0  // scale ADC clock so it's 50-200 kHz
      ;
  ADCSRB = 0;           // free running
  ADCSRA |= 1 << ADSC;  // start conversion

  for (;;) {
    const uint16_t knob = ADC;
    segdisp_show_number(knob);
    segdisp_set_brightness(knob >> 2);

    // const float knob_float = knob / 1023.0f;
    // const float brightness = powf(2.0f, knob_float) - 1;
    // segdisp_set_brightness(brightness * 255.0f + 0.5f);

    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();
  }

  return 0;
}
