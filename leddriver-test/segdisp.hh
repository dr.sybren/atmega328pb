/* ATmega328pb TLC6C598QPWRQ1 8-channel LED driver for segment display.
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

// Pins:
//  DIGIT3: PB0
//  DIGIT2: PB1
//  MOSI  : PB3
//  SCK   : PB5
//  DIGIT1: BP7
//  DIGIT_DISABLE: PD5
//  DIGIT_REGISTER: PD6
//  DIGIT4: PD7
// Timers:
//  Timer 0 for PWM of the DIGIT_DISABLE pin.

#include <stdint.h>

void segdisp_setup();
void segdisp_set_brightness(uint8_t brightness);
void segdisp_show_number(uint16_t number);
void segdisp_set_decpoint(uint8_t position);
