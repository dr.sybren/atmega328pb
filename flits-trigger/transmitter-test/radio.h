/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include "nrf24l01.h"

#include <stdbool.h>
#include <stdint.h>

extern volatile bool radio_irq_seen;
extern nRF24L01 *rf;

typedef union {
  struct {
    uint8_t reserved0 : 1;  // Reserved & undocumented
    uint8_t rx_p_no : 3;    // Data pipe number for the payload
    uint8_t max_rt : 1;  // Maximum number of TX retries interrupt Write 1 to clear bit. If MAX_RT
                         // is set it must be cleared to enable further communication.
    uint8_t tx_ds : 1;   // Data Sent TX FIFO interrupt. Set high when packet sent on TX. If
                         // AUTO_ACK is activated, this bit will be set high only when ACK is
                         // received. Write 1 to clear bit
    uint8_t rx_dr : 1;   // Data Ready RX FIFO interrupt. Set high when new data arrives RX FIFO.
                         // Write 1 to clear bit.
    uint8_t reserved7 : 1;  // Only '0' allowed
  };
  uint8_t byte;
} RadioStatus;

void radio_setup();
bool radio_can_reach_ic();
