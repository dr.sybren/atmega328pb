/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "radio.h"
#include "f_cpu.h"
#include "nrf24l01-mnemonics.h"
#include "nrf24l01.h"
#include "pins.h"

#include <avr/interrupt.h>
#include <avr/io.h>

#include <stdint.h>

volatile bool radio_irq_seen;
nRF24L01 *rf;

void radio_setup()
{
  static nRF24L01 radio;
  rf = &radio;

  nRF24L01_init(rf);

  rf->ss.port = &PORTC;
  rf->ss.pin = PC4;
  rf->ce.port = &PORTC;
  rf->ce.pin = PC3;
  rf->sck.port = &PORTC;
  rf->sck.pin = PC1;
  rf->mosi.port = &PORTE;
  rf->mosi.pin = PE3;
  rf->miso.port = &PORTC;
  rf->miso.pin = PC0;

  // interrupt on falling edge of PCINT10 (PC2)
  PCICR |= _BV(PCIE1);
  PCMSK1 |= _BV(PCINT10);

  nRF24L01_begin(rf);
}

// nRF24L01 IRQ interrupt
ISR(PCINT1_vect)
{
  // IRQ is active LOW.
  if (pin_read_radio_irq())
    return;

  // pin_write_led_red(true);
  radio_irq_seen = true;
}

bool radio_can_reach_ic()
{
  uint8_t config = 0;
  const uint8_t radio_status = nRF24L01_read_register(rf, CONFIG, &config, 1);
  return radio_status != 0xff && config != 0xff;
}
