/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <stdbool.h>

#include <avr/io.h>

/* Pinout:
 * PD2: IN  SYNC
 * PE2: OUT LED1
 * PD5: OUT LED2 (OC0B)
 *
 * For the transceiver, configuration taken care of by nrf24l01 library:
 * PC0: IN  MISO1
 * PC1: OUT SCK1
 * PC2: IN  IRQ
 * PC3: OUT CE
 * PC4: OUT CSN
 * PE3: IN  MOSI1
 */

extern bool sync_received;

void pins_setup();
void pins_slow_error();

#define PIN_WRITE_FUNC(name, port, pin) \
  static inline void name(const bool pin_state) \
  { \
    if (pin_state) { \
      port |= 1 << port##pin; \
    } \
    else { \
      port &= ~(1 << port##pin); \
    } \
  }

#define PIN_READ_FUNC(name, port, pin) \
  static inline bool name() \
  { \
    return port & (1 << port##pin); \
  }

PIN_WRITE_FUNC(pin_write_led_red, PORTE, 2)
PIN_WRITE_FUNC(pin_write_led_green, PORTD, 5)
PIN_WRITE_FUNC(pin_write_radio_ce, PORTC, 3)

PIN_READ_FUNC(pin_read_sync, PIND, 2)
PIN_READ_FUNC(pin_read_led_red, PINE, 2)
PIN_READ_FUNC(pin_read_radio_irq, PINC, 2)

/* Brightness on scale 0 (off) to 48 (brightest). */
static const uint8_t GREEN_BRIGHTNESS_IDLE = 1;
static const uint8_t GREEN_BRIGHTNESS_MAX = 255;
void pin_write_led_green_brightness(uint8_t brightness);
