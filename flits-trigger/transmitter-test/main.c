#include "f_cpu.h"
#include "nrf24l01-mnemonics.h"
#include "nrf24l01.h"
#include "pins.h"
#include "radio.h"

#include <stdbool.h>
#include <string.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <util/delay.h>

volatile bool send_message = false;

const uint8_t ADDRESS_SIZE = 5;
uint8_t ADDR_TRANSMIT[5] = {0xde, 0xad, 0xbe, 0xef, 0x01};
uint8_t ADDR_RECEIVE[5] = {0xde, 0xad, 0xbe, 0xef, 0x47};

void delay_cycles(const uint8_t cycle_count)
{
  for (uint8_t count = 0; count < cycle_count; ++count)
    _delay_ms(32);
}

#pragma GCC push_options
#pragma GCC optimize("O0")
int main(void)
{
  f_cpu_setup();
  pins_setup();
  sei();

  pin_write_led_green_brightness(GREEN_BRIGHTNESS_IDLE);
  pin_write_led_red(true);

  radio_setup();

  while (!radio_can_reach_ic()) {
    pins_slow_error();
  }

  nRF24L01_send_command(rf, FLUSH_RX, NULL, 0);
  nRF24L01_send_command(rf, FLUSH_TX, NULL, 0);
  nRF24L01_clear_interrupts(rf);

  nRF24L01_write_register_byte(rf, EN_AA, 0b000011);         // enable Auto Acknowledge on pipe 0
  nRF24L01_write_register_byte(rf, DYNPD, 0b111111);         // enable Dynamic Payload on all pipes
  nRF24L01_write_register_byte(rf, FEATURE, 1 << EN_DPL);    // enable Dynamic Payload (global)
  nRF24L01_write_register_byte(rf, EN_RXADDR, 1 << ERX_P0);  // enable RX on pipe 0
  nRF24L01_write_register_byte(rf, SETUP_AW, 0b11);          // 5-bit address width
  nRF24L01_write_register_byte(rf, RF_CH, 0b0000010);        // Set RF channel number

  // Set up Auto Retry Delay and Auto Retry (max) Count.
  nRF24L01_write_register_byte(rf, SETUP_RETR, (0b1111 << ARD) | (0xB << ARC));
  // Configure 1Mbit/sec speed
  nRF24L01_write_register_byte(rf, RF_SETUP, 0 << RF_DR_LOW | 0 << RF_DR_HIGH | 0b11 << RF_PWR);

  // Set addresses
  uint8_t addr[5];
  memcpy(addr, ADDR_TRANSMIT, ADDRESS_SIZE);
  nRF24L01_write_register(rf, RX_ADDR_P0, addr, ADDRESS_SIZE);

  memcpy(addr, ADDR_TRANSMIT, ADDRESS_SIZE);
  nRF24L01_write_register(rf, TX_ADDR, addr, ADDRESS_SIZE);

  // Payload Width
  char payload[6];
  nRF24L01_write_register_byte(rf, RX_PW_P0, sizeof(payload));

  // Power on radio
  nRF24L01_write_register_byte(rf, CONFIG, _BV(EN_CRC) | _BV(CRCO) | _BV(PWR_UP));

  nRF24L01_clear_transmit_interrupts(rf);

  // setup timer 1 to trigger interrupt every second when at 1MHz
  // WGM = 0100 for CTC mode
  TCCR1A = 0b00 << WGM10;
  TCCR1B = 0b01 << WGM12 | 0b100 << CS10;
  TIMSK1 = 1 << OCIE1A;
  OCR1A = 65535;

  pin_write_led_red(false);

  for (;;) {
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();

    if (send_message) {
      pin_write_led_green_brightness(GREEN_BRIGHTNESS_MAX);
      pin_write_led_red(false);

      nRF24L01_clear_transmit_interrupts(rf);

      // Send the payload
      memcpy(payload, "FLASH", sizeof(payload));
      nRF24L01_send_command(rf, W_TX_PAYLOAD, payload, sizeof(payload));

      // Pulse CE to initiate transmission
      nRF24L01_pulse_ce(rf);

      delay_cycles(1);
      pin_write_led_green_brightness(0);

      send_message = false;
    }

    if (radio_irq_seen) {
      radio_irq_seen = false;

      // const uint8_t observe_tx = nRF24L01_read_register_byte(rf, OBSERVE_TX);
      uint8_t status;
      status = nRF24L01_no_op(rf);

      // Clear the interrupts
      nRF24L01_write_register_byte(rf, STATUS, _BV(TX_DS) | _BV(MAX_RT));

      if (status & _BV(TX_DS)) {
        // Transmission OK
        pin_write_led_red(true);
        delay_cycles(5);
        pin_write_led_red(false);

        pin_write_led_green_brightness(GREEN_BRIGHTNESS_IDLE);
      }
      else if (status & _BV(MAX_RT)) {
        // Max retry reached
        pin_write_led_red(true);
        // pin_write_led_green_brightness(GREEN_BRIGHTNESS_IDLE);
        nRF24L01_send_command(rf, FLUSH_TX, NULL, 0);
      }
    }
  }

  return 0;
}
#pragma GCC pop_options

// // each one second interrupt
ISR(TIMER1_COMPA_vect)
{
  send_message = true;
}
