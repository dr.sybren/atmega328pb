#include "f_cpu.h"
#include "nrf24l01-mnemonics.h"
#include "nrf24l01.h"
#include "pins.h"
#include "radio.h"

#include <stdbool.h>
#include <string.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <util/atomic.h>
#include <util/delay.h>

volatile bool check_connectivity = true;  // Send a CHECK packet at startup.

uint8_t ADDR_TRANSMIT[5] = {0xde, 0xad, 0xbe, 0xef, 0x01};
uint8_t ADDR_RECEIVE[5] = {0xde, 0xad, 0xbe, 0xef, 0x47};

static void check_connectivity_setup();
static void delay_connectivity_check();

int main(void)
{
  f_cpu_setup();
  pins_setup();
  check_connectivity_setup();
  sei();

  pin_write_led_green_brightness(GREEN_BRIGHTNESS_IDLE);
  pin_write_led_red(true);

  radio_setup(ADDR_TRANSMIT, sizeof(ADDR_TRANSMIT));
  while (!radio_can_reach_ic()) {
    pins_slow_error();
  }

  // Give the radio time to start up.
  _delay_ms(5);

  pin_write_led_red(false);

  for (;;) {
    // All if-blocks end in 'continue' to avoid the sleep at the end of the for-body.

    if (radio_irq_seen) {
      radio_irq_seen = false;

      // Can be useful to keep track of number of lost packets.
      // const uint8_t observe_tx = nRF24L01_read_register_byte(rf, OBSERVE_TX);

      const eTranmissionStatus success = nRF24L01_transmission_status(rf);
      switch (success) {
        case TRANSMISSION_UNKNOWN:
          // The radio IRQ probably wasn't to signal end of transmission. Not sure it was for, so
          // just don't do anything.
          break;
        case TRANSMISSION_MAX_RT:
          pin_write_led_red(true);
          break;
        case TRANSMISSION_OK:
          pin_write_led_red(false);
          break;
      }

      // Regardless of what happened, just flush the transmit queue.
      nRF24L01_flush_transmit_message(rf);

      pin_write_led_green_brightness(GREEN_BRIGHTNESS_IDLE);

      // Set CONFIG:PRIM_RX low to prepare for next transmission.
      // This is required, not just for debugging.
      nRF24L01_register_clear_bit(rf, CONFIG, ~_BV(PRIM_RX));
      continue;
    }

    if (sync_received) {
      sync_received = false;
      nRF24L01_transmit(rf, (uint8_t *)"FLASH", 6);
      pin_write_led_green_brightness(GREEN_BRIGHTNESS_MAX);

      // Be sure to avoid sending a connectivity check too quickly after this.
      delay_connectivity_check();
      continue;
    }

    if (check_connectivity) {
      check_connectivity = false;
      nRF24L01_transmit(rf, (uint8_t *)"CHECK", 6);
      pin_write_led_green_brightness(GREEN_BRIGHTNESS_MAX);
      continue;
    }

    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();
  }

  return 0;
}

// Setup timer1 to trigger interrupt at 1/2 Hz when at 8MHz
static void check_connectivity_setup()
{
  // WGM = 0b0100 for CTC mode
  TCCR1A = 0b00 << WGM10;
  TCCR1B = 0b01 << WGM12 | 0b101 << CS10;
  TCCR1C = 0;
  TIMSK1 = 1 << OCIE1A;
  OCR1A = 15624;
}

// Interrupt every 2 seconds
ISR(TIMER1_COMPA_vect)
{
  check_connectivity = true;
}

static void delay_connectivity_check()
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    check_connectivity = false;
    TCNT1 = 0;
  }
}
