/**
 * The MIT License (MIT)
 *
 * Original Copyright (c) 2014 Antoine Leclair
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Source: https://github.com/antoineleclair/avr-nrf24l01
 *
 */

#include "nrf24l01.h"
#include "f_cpu.h"
#include "nrf24l01-mnemonics.h"

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <avr/io.h>
#include <util/delay.h>

static void copy_address(const uint8_t *const source, uint8_t *destination);
inline static void set_as_output(gpio_pin pin);
inline static void set_high(gpio_pin pin);
inline static void set_low(gpio_pin pin);
static void spi_init(nRF24L01 *rf);
static uint8_t spi_transfer(uint8_t data);

nRF24L01 *nRF24L01_init(nRF24L01 *rf)
{
  memset(rf, 0, sizeof(nRF24L01));
  return rf;
}

void nRF24L01_begin(nRF24L01 *rf)
{
  set_as_output(rf->ss);
  set_as_output(rf->ce);

  set_high(rf->ss);
  set_low(rf->ce);

  spi_init(rf);
}

uint8_t nRF24L01_send_command(nRF24L01 *rf, uint8_t command, void *data, size_t length)
{
  set_low(rf->ss);

  rf->status = spi_transfer(command);
  for (unsigned int i = 0; i < length; i++)
    ((uint8_t *)data)[i] = spi_transfer(((uint8_t *)data)[i]);

  set_high(rf->ss);

  return rf->status;
}

uint8_t nRF24L01_send_command_ro(nRF24L01 *rf,
                                 uint8_t command,
                                 const uint8_t *const data,
                                 size_t length)
{
  set_low(rf->ss);

  rf->status = spi_transfer(command);
  for (unsigned int i = 0; i < length; i++)
    spi_transfer(data[i]);

  set_high(rf->ss);

  return rf->status;
}

uint8_t nRF24L01_write_register(nRF24L01 *rf, uint8_t reg_address, void *data, size_t length)
{
  return nRF24L01_send_command(rf, W_REGISTER | reg_address, data, length);
}

uint8_t nRF24L01_write_register_byte(nRF24L01 *rf, uint8_t reg_address, uint8_t byte)
{
  return nRF24L01_send_command(rf, W_REGISTER | reg_address, &byte, 1);
}

uint8_t nRF24L01_read_register(nRF24L01 *rf, uint8_t reg_address, void *data, size_t length)
{
  return nRF24L01_send_command(rf, R_REGISTER | reg_address, data, length);
}

uint8_t nRF24L01_read_register_byte(nRF24L01 *rf, uint8_t reg_address)
{
  uint8_t byte;
  nRF24L01_send_command(rf, R_REGISTER | reg_address, &byte, 1);
  return byte;
}

uint8_t nRF24L01_register_set_bit(nRF24L01 *rf,
                                  const uint8_t reg_address,
                                  const uint8_t bitmask_set1)
{
  uint8_t register_value = nRF24L01_read_register_byte(rf, reg_address);
  register_value |= bitmask_set1;
  nRF24L01_write_register_byte(rf, reg_address, register_value);
  return register_value;
}

uint8_t nRF24L01_register_clear_bit(nRF24L01 *rf,
                                    const uint8_t reg_address,
                                    const uint8_t bitmask_clear0)
{
  uint8_t register_value = nRF24L01_read_register_byte(rf, reg_address);
  register_value &= bitmask_clear0;
  nRF24L01_write_register_byte(rf, reg_address, register_value);
  return register_value;
}

uint8_t nRF24L01_no_op(nRF24L01 *rf)
{
  return nRF24L01_send_command(rf, NOP, NULL, 0);
}

uint8_t nRF24L01_update_status(nRF24L01 *rf)
{
  return nRF24L01_no_op(rf);
}

bool nRF24L01_data_received(nRF24L01 *rf)
{
  set_low(rf->ce);
  nRF24L01_update_status(rf);
  return nRF24L01_pipe_number_received(rf) >= 0;
}

void nRF24L01_listen(nRF24L01 *rf, int pipe, uint8_t *address)
{
  uint8_t addr[5];
  copy_address(address, addr);

  nRF24L01_write_register(rf, RX_ADDR_P0 + pipe, addr, 5);

  uint8_t current_pipes;
  nRF24L01_read_register(rf, EN_RXADDR, &current_pipes, 1);
  current_pipes |= _BV(pipe);
  nRF24L01_write_register(rf, EN_RXADDR, &current_pipes, 1);

  set_high(rf->ce);
}

bool nRF24L01_read_received_data(nRF24L01 *rf, nRF24L01Message *message)
{
  message->pipe_number = nRF24L01_pipe_number_received(rf);
  nRF24L01_clear_receive_interrupt(rf);
  if (message->pipe_number < 0) {
    message->length = 0;
    return false;
  }

  nRF24L01_read_register(rf, R_RX_PL_WID, &message->length, 1);

  if (message->length > 0) {
    nRF24L01_send_command(rf, R_RX_PAYLOAD, &message->data, message->length);
  }

  return true;
}

int nRF24L01_pipe_number_received(nRF24L01 *rf)
{
  int pipe_number = (rf->status & RX_P_NO_MASK) >> 1;
  return pipe_number <= 5 ? pipe_number : -1;
}

void nRF24L01_transmit(nRF24L01 *rf, const uint8_t *msg, uint8_t msg_length)
{
  nRF24L01_clear_transmit_interrupts(rf);
  nRF24L01_write_register_byte(rf, RX_PW_P0, msg_length);
  nRF24L01_send_command_ro(rf, W_TX_PAYLOAD, msg, msg_length);
  nRF24L01_pulse_ce(rf);
}

eTranmissionStatus nRF24L01_transmission_status(nRF24L01 *rf)
{
  set_low(rf->ce);
  nRF24L01_update_status(rf);
  eTranmissionStatus trans_status;
  if (rf->status & _BV(TX_DS))
    trans_status = TRANSMISSION_OK;
  else if (rf->status & _BV(MAX_RT))
    trans_status = TRANSMISSION_MAX_RT;
  else
    trans_status = TRANSMISSION_UNKNOWN;
  nRF24L01_clear_transmit_interrupts(rf);
  uint8_t config;
  nRF24L01_read_register(rf, CONFIG, &config, 1);
  config |= _BV(PRIM_RX);
  nRF24L01_write_register(rf, CONFIG, &config, 1);
  return trans_status;
}

void nRF24L01_flush_transmit_message(nRF24L01 *rf)
{
  nRF24L01_send_command(rf, FLUSH_TX, NULL, 0);
}

void nRF24L01_retry_transmit(nRF24L01 *rf)
{
  // XXX not sure it works this way, never tested
  uint8_t config;
  nRF24L01_read_register(rf, CONFIG, &config, 1);
  config &= ~_BV(PRIM_RX);
  nRF24L01_write_register(rf, CONFIG, &config, 1);
  set_high(rf->ce);
}

void nRF24L01_clear_interrupts(nRF24L01 *rf)
{
  uint8_t data = _BV(RX_DR) | _BV(TX_DS) | _BV(MAX_RT);
  nRF24L01_write_register(rf, STATUS, &data, 1);
}

uint8_t nRF24L01_clear_transmit_interrupts(nRF24L01 *rf)
{
  uint8_t data = _BV(TX_DS) | _BV(MAX_RT);
  return nRF24L01_write_register(rf, STATUS, &data, 1);
}

void nRF24L01_clear_receive_interrupt(nRF24L01 *rf)
{
  uint8_t data = _BV(RX_DR) | rf->status;
  nRF24L01_write_register(rf, STATUS, &data, 1);
}

void nRF24L01_pulse_ce(nRF24L01 *rf)
{
  set_high(rf->ce);
  _delay_us(25);
  set_low(rf->ce);
}

static void copy_address(const uint8_t *const source, uint8_t *destination)
{
  for (int i = 0; i < 5; i++)
    destination[i] = source[i];
}

inline static void set_as_output(gpio_pin pin)
{
  volatile uint8_t *ddr = pin.port - 1;
  *ddr |= _BV(pin.pin);
}

inline static void set_as_input(gpio_pin pin)
{
  volatile uint8_t *ddr = pin.port - 1;
  *ddr &= ~_BV(pin.pin);
}

inline static void set_high(gpio_pin pin)
{
  *pin.port |= _BV(pin.pin);
}

inline static void set_low(gpio_pin pin)
{
  *pin.port &= ~_BV(pin.pin);
}

static void spi_init(nRF24L01 *rf)
{
  // set as master
  SPCR1 |= _BV(MSTR1);
  // enable SPI
  SPCR1 |= _BV(SPE1);
  // MISO pin automatically overrides to input
  set_as_output(rf->sck);
  set_as_output(rf->mosi);
  set_as_input(rf->miso);
  // SPI mode 0: Clock Polarity CPOL = 0, Clock Phase CPHA = 0
  SPCR1 &= ~_BV(CPOL1);
  SPCR1 &= ~_BV(CPHA1);
  // Clock 2X speed
  SPCR1 &= ~_BV(SPR10);
  SPCR1 &= ~_BV(SPR11);
  SPSR1 |= _BV(SPI2X1);
  // most significant first (MSB)
  SPCR1 &= ~_BV(DORD1);
}

static uint8_t spi_transfer(uint8_t data)
{
  SPDR1 = data;
  while (!(SPSR1 & _BV(SPIF1)))
    ;
  return SPDR1;
}
