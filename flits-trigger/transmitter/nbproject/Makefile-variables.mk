#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# ISP configuration
CND_ARTIFACT_DIR_ISP=dist/ISP/production
CND_ARTIFACT_NAME_ISP=transmitter.production.hex
CND_ARTIFACT_PATH_ISP=dist/ISP/production/transmitter.production.hex
CND_PACKAGE_DIR_ISP=${CND_DISTDIR}/ISP/package
CND_PACKAGE_NAME_ISP=transmitter.tar
CND_PACKAGE_PATH_ISP=${CND_DISTDIR}/ISP/package/transmitter.tar
# debugWIRE configuration
CND_ARTIFACT_DIR_debugWIRE=dist/debugWIRE/production
CND_ARTIFACT_NAME_debugWIRE=transmitter.production.hex
CND_ARTIFACT_PATH_debugWIRE=dist/debugWIRE/production/transmitter.production.hex
CND_PACKAGE_DIR_debugWIRE=${CND_DISTDIR}/debugWIRE/package
CND_PACKAGE_NAME_debugWIRE=transmitter.tar
CND_PACKAGE_PATH_debugWIRE=${CND_DISTDIR}/debugWIRE/package/transmitter.tar
