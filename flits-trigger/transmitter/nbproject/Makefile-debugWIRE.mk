#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-debugWIRE.mk)" "nbproject/Makefile-local-debugWIRE.mk"
include nbproject/Makefile-local-debugWIRE.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=debugWIRE
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/transmitter.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/transmitter.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=f_cpu.c main.c nrf24l01.c pins.c radio.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/f_cpu.o ${OBJECTDIR}/main.o ${OBJECTDIR}/nrf24l01.o ${OBJECTDIR}/pins.o ${OBJECTDIR}/radio.o
POSSIBLE_DEPFILES=${OBJECTDIR}/f_cpu.o.d ${OBJECTDIR}/main.o.d ${OBJECTDIR}/nrf24l01.o.d ${OBJECTDIR}/pins.o.d ${OBJECTDIR}/radio.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/f_cpu.o ${OBJECTDIR}/main.o ${OBJECTDIR}/nrf24l01.o ${OBJECTDIR}/pins.o ${OBJECTDIR}/radio.o

# Source Files
SOURCEFILES=f_cpu.c main.c nrf24l01.c pins.c radio.c

# Pack Options 
PACK_COMPILER_OPTIONS=-I "${DFP_DIR}/include"
PACK_COMMON_OPTIONS=-B "${DFP_DIR}/gcc/dev/atmega328pb"



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-debugWIRE.mk dist/${CND_CONF}/${IMAGE_TYPE}/transmitter.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=ATmega328PB
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/f_cpu.o: f_cpu.c  .generated_files/fee63a8948f0518dd28d8a1fd108e418709d243e.flag .generated_files/3d12c49a3ad9fd2dd384e17b29fc1ccbeab849dc.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/f_cpu.o.d 
	@${RM} ${OBJECTDIR}/f_cpu.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega328pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/f_cpu.o.d" -MT "${OBJECTDIR}/f_cpu.o.d" -MT ${OBJECTDIR}/f_cpu.o  -o ${OBJECTDIR}/f_cpu.o f_cpu.c  -DXPRJ_debugWIRE=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/main.o: main.c  .generated_files/f13a7258a27195ef69de4e6a83818941bfc54a1b.flag .generated_files/3d12c49a3ad9fd2dd384e17b29fc1ccbeab849dc.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega328pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/main.o.d" -MT "${OBJECTDIR}/main.o.d" -MT ${OBJECTDIR}/main.o  -o ${OBJECTDIR}/main.o main.c  -DXPRJ_debugWIRE=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/nrf24l01.o: nrf24l01.c  .generated_files/c7b1ebb07dde463478be26078333e4897697b094.flag .generated_files/3d12c49a3ad9fd2dd384e17b29fc1ccbeab849dc.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/nrf24l01.o.d 
	@${RM} ${OBJECTDIR}/nrf24l01.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega328pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/nrf24l01.o.d" -MT "${OBJECTDIR}/nrf24l01.o.d" -MT ${OBJECTDIR}/nrf24l01.o  -o ${OBJECTDIR}/nrf24l01.o nrf24l01.c  -DXPRJ_debugWIRE=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/pins.o: pins.c  .generated_files/71bf5e0bec09b06249853b5eb135e97feab3243a.flag .generated_files/3d12c49a3ad9fd2dd384e17b29fc1ccbeab849dc.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/pins.o.d 
	@${RM} ${OBJECTDIR}/pins.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega328pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/pins.o.d" -MT "${OBJECTDIR}/pins.o.d" -MT ${OBJECTDIR}/pins.o  -o ${OBJECTDIR}/pins.o pins.c  -DXPRJ_debugWIRE=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/radio.o: radio.c  .generated_files/ab4e1b9745da6f9e5d6a2c263925dc52597cba81.flag .generated_files/3d12c49a3ad9fd2dd384e17b29fc1ccbeab849dc.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/radio.o.d 
	@${RM} ${OBJECTDIR}/radio.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega328pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/radio.o.d" -MT "${OBJECTDIR}/radio.o.d" -MT ${OBJECTDIR}/radio.o  -o ${OBJECTDIR}/radio.o radio.c  -DXPRJ_debugWIRE=$(CND_CONF)  $(COMPARISON_BUILD) 
	
else
${OBJECTDIR}/f_cpu.o: f_cpu.c  .generated_files/61d5191a073806e6cac4439141bb64f2ca15a57b.flag .generated_files/3d12c49a3ad9fd2dd384e17b29fc1ccbeab849dc.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/f_cpu.o.d 
	@${RM} ${OBJECTDIR}/f_cpu.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega328pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/f_cpu.o.d" -MT "${OBJECTDIR}/f_cpu.o.d" -MT ${OBJECTDIR}/f_cpu.o  -o ${OBJECTDIR}/f_cpu.o f_cpu.c  -DXPRJ_debugWIRE=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/main.o: main.c  .generated_files/38e40849999532c3016e85696093db48e0638760.flag .generated_files/3d12c49a3ad9fd2dd384e17b29fc1ccbeab849dc.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega328pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/main.o.d" -MT "${OBJECTDIR}/main.o.d" -MT ${OBJECTDIR}/main.o  -o ${OBJECTDIR}/main.o main.c  -DXPRJ_debugWIRE=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/nrf24l01.o: nrf24l01.c  .generated_files/59da3a652c0d3714c5e075188d645953b88e7764.flag .generated_files/3d12c49a3ad9fd2dd384e17b29fc1ccbeab849dc.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/nrf24l01.o.d 
	@${RM} ${OBJECTDIR}/nrf24l01.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega328pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/nrf24l01.o.d" -MT "${OBJECTDIR}/nrf24l01.o.d" -MT ${OBJECTDIR}/nrf24l01.o  -o ${OBJECTDIR}/nrf24l01.o nrf24l01.c  -DXPRJ_debugWIRE=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/pins.o: pins.c  .generated_files/3cf587434cad39c3992a8c12d1e3f07a6bfc9e9b.flag .generated_files/3d12c49a3ad9fd2dd384e17b29fc1ccbeab849dc.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/pins.o.d 
	@${RM} ${OBJECTDIR}/pins.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega328pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/pins.o.d" -MT "${OBJECTDIR}/pins.o.d" -MT ${OBJECTDIR}/pins.o  -o ${OBJECTDIR}/pins.o pins.c  -DXPRJ_debugWIRE=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/radio.o: radio.c  .generated_files/751239a0bdc309e7fe7c659ad643a3a4b11cba55.flag .generated_files/3d12c49a3ad9fd2dd384e17b29fc1ccbeab849dc.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/radio.o.d 
	@${RM} ${OBJECTDIR}/radio.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega328pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/radio.o.d" -MT "${OBJECTDIR}/radio.o.d" -MT ${OBJECTDIR}/radio.o  -o ${OBJECTDIR}/radio.o radio.c  -DXPRJ_debugWIRE=$(CND_CONF)  $(COMPARISON_BUILD) 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/transmitter.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mmcu=atmega328pb ${PACK_COMMON_OPTIONS}   -gdwarf-2 -D__$(MP_PROCESSOR_OPTION)__  -Wl,-Map="dist\${CND_CONF}\${IMAGE_TYPE}\transmitter.${IMAGE_TYPE}.map"    -o dist/${CND_CONF}/${IMAGE_TYPE}/transmitter.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}      -DXPRJ_debugWIRE=$(CND_CONF)  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1 -Wl,--gc-sections -Wl,--start-group  -Wl,-lm -Wl,--end-group 
	
	
	
	
	
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/transmitter.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mmcu=atmega328pb ${PACK_COMMON_OPTIONS}  -D__$(MP_PROCESSOR_OPTION)__  -Wl,-Map="dist\${CND_CONF}\${IMAGE_TYPE}\transmitter.${IMAGE_TYPE}.map"    -o dist/${CND_CONF}/${IMAGE_TYPE}/transmitter.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}      -DXPRJ_debugWIRE=$(CND_CONF)  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION) -Wl,--gc-sections -Wl,--start-group  -Wl,-lm -Wl,--end-group 
	${MP_CC_DIR}\\avr-objcopy -O ihex "dist/${CND_CONF}/${IMAGE_TYPE}/transmitter.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}" "dist/${CND_CONF}/${IMAGE_TYPE}/transmitter.${IMAGE_TYPE}.hex"
	
	
	
	
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/debugWIRE
	${RM} -r dist/debugWIRE

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
