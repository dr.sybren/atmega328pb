/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "radio.h"
#include "f_cpu.h"
#include "nrf24l01-mnemonics.h"
#include "nrf24l01.h"
#include "pins.h"

#include <avr/interrupt.h>
#include <avr/io.h>

#include <stdint.h>
#include <string.h>  // for memset()

volatile bool radio_irq_seen;
nRF24L01 *rf;

static void radio_configure_pins(nRF24L01 *rf);
static void radio_configure_transmitter(const uint8_t *address, uint8_t address_size);

void radio_setup(const uint8_t *address, const uint8_t address_size)
{
  static nRF24L01 radio;
  rf = &radio;

  nRF24L01_init(rf);
  radio_configure_pins(rf);

  // interrupt on falling edge of PCINT10 (PC2)
  PCICR |= _BV(PCIE1);
  PCMSK1 |= _BV(PCINT10);

  nRF24L01_begin(rf);
  radio_configure_transmitter(address, address_size);
}

static void radio_configure_pins(nRF24L01 *rf)
{
  rf->ss.port = &PORTC;
  rf->ss.pin = PC4;
  rf->ce.port = &PORTC;
  rf->ce.pin = PC3;
  rf->sck.port = &PORTC;
  rf->sck.pin = PC1;
  rf->mosi.port = &PORTE;
  rf->mosi.pin = PE3;
  rf->miso.port = &PORTC;
  rf->miso.pin = PC0;
}

static void radio_configure_transmitter(const uint8_t *address, uint8_t address_size)
{
  nRF24L01_send_command(rf, FLUSH_RX, NULL, 0);
  nRF24L01_send_command(rf, FLUSH_TX, NULL, 0);
  nRF24L01_clear_interrupts(rf);

  nRF24L01_write_register_byte(rf, EN_AA, 0b000011);         // enable Auto Acknowledge on pipe 0
  nRF24L01_write_register_byte(rf, DYNPD, 0b111111);         // enable Dynamic Payload on all pipes
  nRF24L01_write_register_byte(rf, FEATURE, 1 << EN_DPL);    // enable Dynamic Payload (global)
  nRF24L01_write_register_byte(rf, EN_RXADDR, 1 << ERX_P0);  // enable RX on pipe 0
  nRF24L01_write_register_byte(rf, RF_CH, 0b0000010);        // Set RF channel number

  uint8_t addr_width_register_value = 0b00;  // illegal value according to the spec sheet
  switch (address_size) {
    case 3:
      addr_width_register_value = 0b01;
      break;
    case 4:
      addr_width_register_value = 0b10;
      break;
    case 5:
      addr_width_register_value = 0b11;
      break;
  }
  nRF24L01_write_register_byte(rf, SETUP_AW, addr_width_register_value);

  // Set up Auto Retry Delay and Auto Retry (max) Count.
  // Retry every 1500μs to support all ACK payload sizes at all comms speeds.
  nRF24L01_write_register_byte(rf, SETUP_RETR, (15 << ARD) | (15 << ARC));
  // Configure 1Mbit/sec speed
  // nRF24L01_write_register_byte(rf, RF_SETUP, 0 << RF_DR_LOW | 0 << RF_DR_HIGH | 0b11 << RF_PWR);
  // Configure 250kbit/sec speed for max range
  nRF24L01_write_register_byte(rf, RF_SETUP, 1 << RF_DR_LOW | 0 << RF_DR_HIGH | 0b11 << RF_PWR);

  // Set addresses, compatible with Auto ACK.
  uint8_t addr[5];
  memcpy(addr, address, address_size);
  nRF24L01_write_register(rf, RX_ADDR_P0, addr, address_size);

  memcpy(addr, address, address_size);
  nRF24L01_write_register(rf, TX_ADDR, addr, address_size);

  // Power on radio in transmit mode (PRIM_RX low)
  nRF24L01_write_register_byte(rf, CONFIG, _BV(EN_CRC) | _BV(CRCO) | _BV(PWR_UP));

  nRF24L01_clear_transmit_interrupts(rf);
}

// nRF24L01 IRQ interrupt
ISR(PCINT1_vect)
{
  // IRQ is active LOW.
  if (!pin_read_radio_irq())
    radio_irq_seen = true;
}

bool radio_can_reach_ic()
{
  uint8_t config = 0;
  const uint8_t radio_status = nRF24L01_read_register(rf, CONFIG, &config, 1);
  return radio_status != 0xff && config != 0xff;
}
