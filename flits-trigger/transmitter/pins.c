/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "pins.h"
#include "f_cpu.h"

#include <avr/interrupt.h>
#include <util/delay.h>

bool sync_received = false;

void pins_setup()
{
  DDRD = (1 << DDRD5);  // OUT on PD5
  DDRE = (1 << DDRE2);  // OUT on PE2
  PORTD = 1 << PORTD2;  // Pull-up on SYNC

  // Set up Timer0 for PWMing the green LED, but don't connect it to the output yet.
  // WGM = 0b011 for Fast PWM.
  TCCR0A = 0b11 << WGM00;
  TCCR0B = 0b0 << WGM02 | 0b010 << CS00;

  // Wait until pull-up is in effect, illuminating the LED while we wait.
  pin_write_led_red(true);
  while (!pin_read_sync())
    ;
  pin_write_led_red(false);

  EIMSK = 1 << INT0;      // Enable external interrupt on the SYNC pin (PD2=INT0).
  EICRA = 0b10 << ISC00;  // Respond to falling edge.
}

ISR(INT0_vect)
{
  // Pin has pull-up, so low value means 'sync now'.
  if (pin_read_sync())
    return;
  sync_received = true;
}

void pins_slow_error()
{
  for (uint8_t count = 0; count < 3; ++count) {
    pin_write_led_red(true);
    for (uint8_t delays = 0; delays < 25; ++delays)
      _delay_ms(10);
    pin_write_led_red(false);
    for (uint8_t delays = 0; delays < 25; ++delays)
      _delay_ms(10);
  }
}

void pin_write_led_green_brightness(const uint8_t brightness)
{
  static const uint8_t light_map_8bit[] = {
      0,  1,  2,  3,  4,   5,   6,   7,   8,   9,   10,  11,  12,  13,  15, 16,
      18, 20, 21, 24, 26,  28,  31,  34,  37,  41,  45,  49,  53,  58,  64, 69,
      76, 83, 90, 98, 107, 117, 128, 139, 152, 166, 181, 197, 215, 234, 255};
  static const uint8_t light_map_8bit_size = 47;

  if (brightness == 0 || brightness > light_map_8bit_size) {
    TCCR0A &= ~(0b11 << COM0B0);  // disable PWM output
    pin_write_led_green(brightness > 0);
    return;
  }

  const uint8_t pwm_value = light_map_8bit[brightness - 1];
  OCR0B = pwm_value;
  TCCR0A |= (0b10 << COM0B0);  // enable PWM output
}
