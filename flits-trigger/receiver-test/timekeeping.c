/* ATmega328pb Flash Receiver
 * Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "timekeeping.h"
#include "pins.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

volatile time_csec_t centisec_since_reset = 0;

void timekeeping_setup()
{
  PRR1 &= (1 << PRTIM3);

  // WGM = 0b0100 for CTC mode.
  TCCR3A = 0                //
           | 0b00 << WGM30  //
      ;
  TCCR3B = 0                //
           | 0b01 << WGM32  //
           | 0b101 << CS30  // 1/1024 clockdiv
      ;
  OCR3A = 77;  // should give 100.2 Hz
  TIMSK3 = 1 << OCIE3A;
  TCNT3 = 0;

  timekeeping_reset();
}

ISR(TIMER3_COMPA_vect)
{
  ++centisec_since_reset;
}

time_csec_t timekeeping_centisec()
{
  return centisec_since_reset;
}

void timekeeping_reset()
{
  centisec_since_reset = 0;
}

void timekeeping_delay_csec(const time_csec_t centiseconds)
{
  const time_csec_t wait_start = timekeeping_centisec();
  while (timekeeping_centisec() - wait_start < centiseconds) {
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();
  }
}
