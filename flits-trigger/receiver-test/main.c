#include "f_cpu.h"
#include "pins.h"
#include "timekeeping.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

int main(void)
{
  pins_setup();
  pin_write_power_enable(true);

  f_cpu_setup();
  timekeeping_setup();

  // Timer 0 controls the LEDs, WGM = 0b011 for fast PWM.
  TCCR0A = 0                 //
           | 0b10 << COM0A0  // Normal mode PWM
                             //  | 0b10 << COM0B0  // Normal mode PWM
           | 0b11 << WGM00   //
      ;
  TCCR0B = 0                //
           | 0b0 << WGM02   //
           | 0b001 << CS00  // Clockdiv
      ;
  TIMSK0 = 1 << TOIE0;
  GTCCR = 0;

  sei();

  OCR0A = 0;  // white
  // OCR0B = 1;  // blue

  timekeeping_reset();
  bool led_on = false;
  uint8_t iterations = 0;
  for (;;) {
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();

    if (timekeeping_centisec() >= 100) {
      timekeeping_reset();
      led_on = !led_on;
      pin_write_led_connection(led_on);

      ++iterations;
      if (iterations >= 4) {
        pin_write_power_enable(false);
      }
    }
  }
}

static const uint8_t light_map_8bit[] = {
    0,   1,   2,   3,   4,   5,   6,   7,   8,   9,   9,   10,  11,  11,  12,  13,
    14,  15,  16,  17,  18,  20,  21,  22,  24,  25,  27,  29,  31,  33,  35,  37,
    39,  42,  44,  47,  50,  53,  57,  60,  64,  68,  72,  77,  82,  87,  92,  98,
    104, 110, 117, 125, 132, 140, 149, 158, 168, 178, 189, 201, 213, 226, 240, 255};
static const uint8_t light_map_8bit_size = 64;

ISR(TIMER0_OVF_vect)
{
  static uint16_t counter = 0;
  if (counter++ % 1000)
    return;

  static uint8_t light_map_index = 0;
  light_map_index = (light_map_index + 1) % light_map_8bit_size;
  OCR0A = light_map_8bit[light_map_index++];
  // OCR0B = 255;

  // pin_write_led_connection(true);
}
