/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <stdbool.h>

#include <avr/io.h>

/* Pinout:
 * PB1: OUT Sync
 * PB2: IN  Sync check
 * PD3: OUT Power enable
 * PD4: IN  Power button
 * PD5: OUT LED "Connection"
 * PD6: OUT LED "Trigger"
 *
 * For the transceiver:
 * PE3: OUT MOSI1
 * PC0: IN  MISO1
 * PC1: OUT SCK1
 * PC2: IN  IRQ
 * PC3: OUT CE
 * PC4: OUT CSN
 */

void pins_setup();

#define PIN_WRITE_FUNC(name, port, pin) \
  static inline void name(const bool pin_state) \
  { \
    if (pin_state) { \
      port |= 1 << port##pin; \
    } \
    else { \
      port &= ~(1 << port##pin); \
    } \
  }

#define PIN_READ_FUNC(name, port, pin) \
  static inline bool name() \
  { \
    return port & (1 << port##pin); \
  }

PIN_WRITE_FUNC(pin_write_power_enable, PORTD, 3)
PIN_WRITE_FUNC(pin_write_led_connection, PORTD, 5)
PIN_WRITE_FUNC(pin_write_led_trigger, PORTD, 6)
PIN_WRITE_FUNC(pin_write_sync, PORTB, 1)

PIN_READ_FUNC(pin_read_sync_check, PINB, 2)
PIN_READ_FUNC(pin_read_power_button, PIND, 4)
PIN_READ_FUNC(pin_read_led_connection, PIND, 5)
