/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "pins.h"

#include <avr/interrupt.h>

void pins_setup()
{
  DDRB = 1 << DDRB1;
  DDRC = 1 << DDRC1 | 1 << DDRC3 | 1 << DDRC4;
  DDRD = 1 << DDRD3 | 1 << DDRD5 | 1 << DDRD6;
  DDRE = 1 << DDRE3;

  PORTB = 0;
  PORTC = 0;
  PORTD = 0;
  PORTE = 0;

  // EIMSK = 1 << INT0;      // Enable external interrupt on the SYNC pin (PD2=INT0).
  // EICRA = 0b10 << ISC00;  // Respond to falling edge.
}

// ISR(INT0_vect)
// {
//   // // Pin has pull-up, so low value means 'sync now'.
//   // if (pin_read_sync())
//   //   return;
//   // sync_received = true;
// }
