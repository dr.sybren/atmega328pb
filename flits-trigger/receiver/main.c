#include "alarms.h"
#include "f_cpu.h"
#include "nrf24l01-mnemonics.h"
#include "nrf24l01.h"
#include "pins.h"
#include "radio.h"
#include "timekeeping.h"

#include <stdbool.h>
#include <string.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <util/delay.h>

uint8_t ADDR_RECEIVE[5] = {0xde, 0xad, 0xbe, 0xef, 0x01};

static void watchdog_setup();
static void do_power_down();

void strobe(void)
{
  pin_write_sync(true);
  pin_write_led_trigger(true);

  _delay_ms(32);

  pin_write_led_trigger(false);
  pin_write_sync(false);
}

void process_message(char *message)
{
  if (!strcmp(message, "FLASH"))
    strobe();
}

int main(void)
{
  pins_setup();

  pin_write_power_enable(true);
  pin_write_led_connection(true);
  pin_write_led_trigger(true);

  watchdog_setup();
  f_cpu_setup();
  timekeeping_setup();
  alarms_setup();

  sei();

  radio_setup(ADDR_RECEIVE, sizeof(ADDR_RECEIVE));

  while (!radio_can_reach_ic()) {
    pins_slow_error();
  }

  uint8_t addr[5];
  nRF24L01_read_register(rf, CONFIG, addr, 1);

  // It takes a while for the power button pin to go down, so wait for it here after the rest of
  // the startup is done and ignore any pin-change since startup.
  while (pin_read_power_button())
    ;
  power_button_pressed = false;

  pin_write_led_connection(false);
  pin_write_led_trigger(false);

  while (true) {
    nRF24L01_listen(rf);

    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();

    if (power_down)
      do_power_down();

    while (radio_irq_seen) {
      pin_write_led_connection(true);
      radio_irq_seen = false;
      nRF24L01_clear_interrupts(rf);

      while (nRF24L01_data_received(rf)) {
        nRF24L01Message msg;
        nRF24L01_read_received_data(rf, &msg);
        process_message((char *)msg.data);

        // Timekeeping tracks when we last saw a message from a transmitter (FLASH or otherwise).
        timekeeping_reset();
      }
      pin_write_led_connection(false);
    }

    if (power_button_pressed) {
      strobe();
      power_button_pressed = false;
    }

    // Transmitter should send a CHECK every 2 seconds, so after 2 we raise the alarm.
    alarms_connectivity(timekeeping_centisec() > 300);

    // The strobe should pull the sync pin high. If that doesn't happen, the strobe got
    // disconnected.
    alarms_wire_disconnect(!pin_read_sync_check());
  }

  return 0;
}

ISR(WDT_vect)
{
}

static void watchdog_setup()
{
  // Slow watchdog for waking up every few seconds.
  MCUSR &= ~(1 << WDRF);
  WDTCSR = 1 << WDCE | 1 << WDE;
  WDTCSR = 1 << WDIE | 0b0110 << WDP0;
}

static void do_power_down()
{
  while (power_down && pin_read_power_button()) {
    pin_write_led_connection(true);
    pin_write_led_trigger(true);
    pin_write_power_enable(false);
  }
  pin_write_led_connection(false);
  pin_write_led_trigger(false);
}
