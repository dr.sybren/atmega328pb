/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "pins.h"
#include "f_cpu.h"

#include <avr/interrupt.h>
#include <util/delay.h>

volatile bool power_button_pressed = false;
volatile bool power_down = false;

void pins_setup()
{
  DDRB = 1 << DDRB1;
  DDRC = 1 << DDRC4 | 1 << DDRC3 | 1 << DDRC1;
  DDRD = 1 << DDRD6 | 1 << DDRD5 | 1 << DDRD3;
  DDRE = 1 << DDRE3;

  PORTB = 0;
  PORTC = 0;
  PORTD = 0;
  PORTE = 0;

  // Pin change interrupt on PD4 / PCINT20 power button
  PCMSK2 |= 1 << PCINT20;
  PCICR |= 1 << PCIE2;

  // Setup timer4 to trigger interrupt.
  // WGM = 0b0100 for CTC mode
  TCCR4A = 0b00 << WGM40;
  TCCR4B = 0b01 << WGM42 | 0b100 << CS10;
  TCCR4C = 0;
  TIMSK4 = 1 << OCIE4A;
  OCR4A = 15624;
}

ISR(PCINT2_vect)
{
  power_button_pressed |= pin_read_power_button();
}

ISR(TIMER4_COMPA_vect)
{
  static uint8_t power_down_ticks = 0;
  if (!pin_read_power_button()) {
    power_down = false;
    power_down_ticks = 0;
    return;
  }
  ++power_down_ticks;
  power_down = (power_down_ticks > 2);
}

void pins_slow_error()
{
  for (uint8_t count = 0; count < 3; ++count) {
    pin_write_led_connection(true);
    for (uint8_t delays = 0; delays < 25; ++delays)
      _delay_ms(10);
    pin_write_led_connection(false);
    for (uint8_t delays = 0; delays < 25; ++delays)
      _delay_ms(10);
  }
}
