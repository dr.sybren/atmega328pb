/* ATmega328pb Flash Receiver
 * Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "alarms.h"
#include "pins.h"

#include <avr/interrupt.h>
#include <avr/io.h>

static volatile bool _connectivity_alarm = false;
static volatile bool _wire_disconnect_alarm = false;
static volatile bool _alarm_led = false;

// Setup timer1 to trigger interrupt at ~5Hz when at 8MHz
void alarms_setup()
{
  // WGM = 0b0100 for CTC mode
  TCCR1A = 0b00 << WGM10;
  TCCR1B = 0b01 << WGM12;  // CS=0, is set by connectivity_alarm()
  TCCR1C = 0;
  TIMSK1 = 1 << OCIE1A;
  OCR1A = 1562;
}

void update_timer()
{
  const bool alarm_enabled = _connectivity_alarm || _wire_disconnect_alarm;
  if (alarm_enabled) {
    TCCR1B |= 0b101 << CS10;
  }
  else {
    // Set CS to 0b000
    TCCR1B &= ~(0b111 << CS10);
  }
}

void alarms_connectivity(const bool alarm_enabled)
{
  if (_connectivity_alarm == alarm_enabled)
    return;
  _connectivity_alarm = alarm_enabled;
  pin_write_led_connection(alarm_enabled);
  update_timer();
}

void alarms_wire_disconnect(const bool alarm_enabled)
{
  if (_wire_disconnect_alarm == alarm_enabled)
    return;
  _wire_disconnect_alarm = alarm_enabled;
  pin_write_led_trigger(alarm_enabled);
  update_timer();
}

ISR(TIMER1_COMPA_vect)
{
  _alarm_led = !_alarm_led;
  if (_connectivity_alarm) {
    pin_write_led_connection(_alarm_led);
  }

  if (_wire_disconnect_alarm) {
    pin_write_led_trigger(_alarm_led);
  }
}
